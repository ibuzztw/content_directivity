from pymongo import MongoClient
import pymongo
import json
from datetime import datetime
from bson import BSON
from bson import json_util
import re
import logging
from pymongo.errors import CursorNotFound, ServerSelectionTimeoutError
# TODO: 把連線資料放到環境變數裡面讀取，不要寫死在這裡
class MongoConnection(object):
    # def __init__(self, host="52.221.109.165", port=28288, db_name='Indexasia', conn_type="local", username='AIEService', password='qXVkCELnmf'): 
    def __init__(self, host="52.221.109.165", port=28288, db_name='Indexasia', conn_type="local", username='ezbrand', password='MUDICS53601116'):
        """
        在開啟認證模式之後，如果將用戶密碼寫入 url 中，默認連接的資料庫是 admin。但其實連接的資料庫並不是 admin，所以使用者名稱和密碼是無效的，所以導致認證失敗
        解決方式：在 url 中指定所要連接的資料庫，或是在選擇資料庫之後 （ db = client['log_fieldmap ']  ）
        """
        # 自動判斷是否為 admin 帳號，改變連線 url
        # if username == 'ezbrand':
        #     uri = "mongodb://{user}:{password}@{host}:{port}/admin?authMechanism=SCRAM-SHA-1".format(user=username, password=password, host=host, port=port, db_name=db_name)
        # else:
        #     uri = "mongodb://{user}:{password}@{host}:{port}/{db_name}?authMechanism=SCRAM-SHA-1".format(user=username, password=password, host=host, port=port, db_name=db_name)
        
        uri = "mongodb+srv://tony:T048P0GSQGE27dSY@bigdata-znaj2.mongodb.net/test?retryWrites=true&w=majority"
        # uri = f"mongodb://{username}:{password}@{host}:{port}/{db_name}?authMechanism=SCRAM-SHA-1"
        self.client = MongoClient(uri, appname = '智能知識庫', readPreference = 'secondaryPreferred')      
        # self.uri = uri
        # self.client = MongoClient(self.uri)
        self.db_name = db_name
        self.db = self.client[self.db_name]
        #logging.info("Connection successful")


    def __enter__(self):
        return self
    def __exit__(self,exc_ty, exc_val, tb):
        self.close()
    def reconnection(self):
        # 可能因為公司換 IP 造成與 mongo 斷線
        self.client = MongoClient(self.uri)
        self.db = self.client[self.db_name]
    def change_db(self, db_name=""):
        self.db_name = db_name
        self.db = self.client[self.db_name] 
    def ensure_index(self, table_name, index=None):
        self.db[table_name].ensure_index([(index, pymongo.GEOSPHERE)])
    def create_table(self, table_name, index=None):
        self.db[table_name].create_index([(index, pymongo.DESCENDING)])
    def get_one(self, table_name, conditions={}, showfield = {}, sort_index=None):
        try:
            single_doc = self.db[table_name].find_one(conditions,showfield and showfield or None, no_cursor_timeout=True)
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            single_doc = self.db[table_name].find_one(conditions,showfield and showfield or None, no_cursor_timeout=True)
        return single_doc
    def get_all(self, table_name, conditions={}, showfield = {}, sort_index='_id', limit=0):
        '''
        抓取全部搜尋的資料
        table_name = collection名稱
        conditions = 搜尋條件，如:{'name': 'johnny', 'age':26}
        showfield = 要顯示那些欄位，如:{'name': 1}、{'age':1, '_id':0}
        '''
        #logging.info("Search: {}".format(str(conditions)))
        try: 
            all_doc = self.db[table_name].find(conditions, showfield and showfield or None, no_cursor_timeout=True)
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            all_doc = self.db[table_name].find(conditions, showfield and showfield or None, no_cursor_timeout=True)
        return all_doc
    def insert_one(self, table_name, value):
        '''
        新增一筆資料
        table_name = collection名稱
        value = {
            'name': 'johnny',
            'age': 26
        }
        '''
        try:
            self.db[table_name].insert(value)
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            self.db[table_name].insert(value)
    def insert_many(self, table_name, value):
        try:
            self.db[table_name].insert_many(value)
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            self.db[table_name].insert_many(value)
    def delete_one(self, table_name, value):
        '''
        刪除資料
        table_name = collection名稱
        value = {
            'name': 'johnny'
        }
        '''
        try:
            self.db[table_name].remove(value)
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            self.db[table_name].remove(value)
    def update_push(self, table_name, where, what, _unset=[]):
        # print where, what
        _update = {"$push": what}
        if _unset:
            _update.update({"$unset": {_u:"" for _u in _unset}})
        try:
            self.db[table_name].update(where, _update, upsert=False)
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            self.db[table_name].update(where, _update, upsert=False)
    def update(self, table_name, where, what, _unset=[]):
        # print where, what
        _update = {"$set": what}
        if _unset:
            _update.update({"$unset": {_u:"" for _u in _unset}})
        try:
            self.db[table_name].update(where, _update, upsert=False)
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            self.db[table_name].update(where, _update, upsert=False)
    def update_multi(self, table_name, where, what, _unset=[]):
        _update = {"$set": what}
        if _unset:
            _update.update({"$unset": {_u:"" for _u in _unset}})
        try:
            self.db[table_name].update(where, _update,upsert=False, multi=True)
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            self.db[table_name].update(where, _update,upsert=False, multi=True)
    def update_upsert(self, table_name, where, what, _unset=[]):
        '''
        更新資料
        table_name = collection名稱
        where =  設定要修改的資料 {    
            'name': 'johnny'
        }
        what = 修改的內容 {
            'age': 26
        }
        '''
        _update = {"$set": what}
        if _unset:
            _update.update({"$unset": {_u:"" for _u in _unset}})
        try:
            self.db[table_name].update(where, _update,upsert=True)
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            self.db[table_name].update(where, _update,upsert=True)
    def map_reduce(self, table_name, mapper, reducer, query, result_table_name):
        myresult = self.db[table_name].map_reduce(mapper, reducer, result_table_name, query)
        return myresult

    def map_reduce_search(self, table_name, mapper, reducer, query, sort_by, sort = -1, limit = 20):
        if sort_by == "distance":
            sort_direction = pymongo.ASCENDING
        else:
            sort_direction = pymongo.DESCENDING
        myresult = self.db[table_name].map_reduce(mapper, reducer,'results', query)
        results = self.db['results'].find().sort("value."+sort_by, sort_direction).limit(limit)
        return results

    def aggregrate_all(self, table_name,conditions=[]):
        try:
            all_doc = self.db[table_name].aggregate(conditions)
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            all_doc = self.db[table_name].aggregate(conditions)
        # 有 ['result'] 的寫法是 3.0 之前的用法
        # all_doc = self.db[table_name].aggregate(conditions)['result']
        return all_doc

    def group(self, table_name, key, condition, initial, reducer):
        all_doc = self.db[table_name].group(key=key, condition=condition, initial=initial, reduce=reducer)
        return all_doc

    def get_distinct(self, table_name, distinct_val):
        all_doc = self.db[table_name].find().distinct(distinct_val)
        # count = len(all_doc)
        # parameter = {'count': count, 'results': all_doc}
        return all_doc

    def get_all_vals(self, table_name, conditions={}, sort_index ='_id'):
        all_doc = self.db[table_name].find(conditions).sort(sort_index, pymongo.DESCENDING)
        return all_doc
        
    def get_paginated_values(self, table_name, conditions={}, sort_index='_id', pageNumber=1):
        """
        TODO: 不要用 skip，改用篩選條件大於 _id
        """
        all_doc = self.db[table_name].find(conditions).sort(sort_index, pymongo.DESCENDING).skip((pageNumber-1)*15).limit(15)
        return all_doc
    def get_count(self, table_name, conditions={}, sort_index='_id'):
        '''
        抓搜尋間的總數
        像sql的 select count(*) from admin where 'name'='johnny'
        table_name = collection名稱
        conditions = 搜尋條件，如:{'name': 'johnny'}
        '''
        try:
            count = self.db[table_name].find(conditions).count()
        except (ServerSelectionTimeoutError, CursorNotFound):
            self.reconnection()
            count = self.db[table_name].find(conditions).count()
        return count
    def close(self):
        self.client.close()


if __name__ == "__main__":

    # 2019/02/11 johnny 支援with語法，自動關閉mongo連線
    # with MongoConnection() as conn:
    #     # conn.change_db('Indexasia')
    #     result = list(conn.get_all(table_name='channel', conditions={'type':'morefb'}, limit=2))


    '''
    操作範例
    from mongo_api import MongoConnection
    db = MongoConnection()
    ## 切換不同table
    db.change_db('whale')
    ## 搜尋
    result = db.get_all('collection_name', {'name': 'johnny'})
    ## 搜尋只顯示name欄位
    result = db.get_all('collection_name', {'name': 'johnny'},{'name':1})
    ## 刪除
    db.delete_one('collection_name', {'name': 'johnny'})
    ## 更新
    db.update_upsert('collection_name', {'name': 'johnny'}, {'age': 27})
    ## 新增
    db.insert_one('collection_name', {'name': 'alan', 'age': 25})
    ## 關閉連線
    db.close()
    '''

    conn = MongoConnection()
    # 方法 1
    result = list(conn.get_all(table_name='channel', conditions={'type':'morefb'}, limit=2))
    # for r in result:
    #     print(r)
        