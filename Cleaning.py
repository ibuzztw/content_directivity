# -*- coding: utf-8 -*-

#!/home/ec2-user/anaconda3/bin/python
import re
import time
# import traceback
import logging
import pandas as pd
import numpy as np
import gc

import concurrent.futures
from multiprocessing import Process, Pool, cpu_count
logging.getLogger('').handlers = []


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s : %(message)s')

logger = logging.getLogger('清理資料')
# logger.disabled = False

# from config import config

class ClearContent(object):
    def __init__(self):
        self.content = ''
        self._compile()

    def _compile(self):
        ''' 事先 compile 好,可以節省時間
        '''

        # url
        self._url = re.compile(r'((轉自:)?(https?|ftp|file)://(www)?)[-A-Za-z0-9+&@#\/%?=~_\|!:,.;●]+[-A-Za-z0-9+&@#\/%=~_\|●]|[-A-Za-z0-9+&@#\/%?=~_|!:,.;]+[-A-Za-z0-9+&@#\/%=~_|].com|https:', flags = re.I)
        # url.search(content)
        # 引用部分
        self.quote = re.compile(r'引用:作者\s?[-A-Za-z0-9+&@#\/%?=~_|!:,.;]*|作者[-A-Za-z0-9+&@#\/%?=~_|!:,.;]*:引用', flags = re.I)

        # pandas 非法字元
        self.ILLEGAL_CHARACTERS_RE = re.compile(r'[\000-\010]|[\013-\014]|[\016-\037]')

        # ptt
        self._ptt = re.compile(r"""(1\.媒體來源:.*|2\.完整新聞標題\:|3\.完整新聞內文:|4\.完整新聞連結 \(或短網址\):.*|5\.備註:|
        |1\.媒體來源.*\n|2\.記者署名.*\n|3\.完整新聞標題.*\n|4\.完整新聞內文.*\n|5\.完整新聞連結 \(或短網址\).*\n|6.備註.*\n|
        |※.+\n|: .+\n|Sent\s?from[a-z0-9\_]*\.|(ftp|http|www)+.+\n|
        |※?\\s?標題沒有寫出來.*?依照板規刪除文章|※?\\s?社論特稿都不能貼!違者退文貼廣告也會被退文喔!|
        |\-+?以下發文提示發文後請勿刪除\-+?|
        |(媒體|原文|影片)來源:?|來自:?|(完整)?(新聞|原文)標題:?|(完整)?(新聞|原文)(內文|內容):?|
        |(完整)?(新聞|原文)連結\\s?\\(或短網址\\):?|(完整)?(新聞|原文)連結:?|備註:?|
        |ＦＢ內容:|(ＦＢ|臉書)連結:|(ＦＢ|臉書)*?卦點說明.*?:(（繁體中文.*?20.*?個字）)?|
        |(推|噓|→).*?:|（編輯:.+?）|是否已讀取發文須知並詳閱版規(\(Y/N\):(yes|Y)?)|已爬文關鍵字:?|
        |是否已讀取發文須知並詳閱版規(\(Y/N\):(yes|Y)?)|已爬文關鍵字:?|
        |發文須知:|1.發文前請詳閱版規、發文須知、禁止品牌討論期間清單|2.若有產品請填寫完整品牌與產品名稱.或至少一次全名|
        |3.為心得性意見交換/版友閒聊；若為詢問請使用標題「請益或挑選」|3.請依實際使用事實闡述心得.切勿進行廣告意圖及抹黑之事|
        |4.請發文後盡量撥空對版友意見進行回覆|5.下述(「是否已讀取發文須知並詳閱版規\(Y/N\)」|「\(Y/N\)」)|
        |如填N.請發文前詳閱版規；填Y表示若違反版規.同意配合本版規章處置|
        |=+?發文前以上可刪除=+?\n|-+?以上請Ctrl\+Y整行刪除（含本行）-+?\n|
        |3.請先查詢精華區、本版文章及至網頁搜尋引擎查詢|4.本版不歡迎.+?皆認列.+?文\)|5.本版非.+?版.如有.+?狀況/用藥指示詢問.請洽.+?不可問診|
        |6.請發文後盡量撥空對版友意見進行回覆|7.下述「是否已讀取發文須知並詳閱版規\(Y/N\)」|8.下述「」請至少列「.+?」的搜尋關鍵字|
        |並於文內詳述需求.+?等細節|1.發文前請先詳閱\[標的\]分類發文規範.(按CTRL\+Y可刪除以上內容.|未依規範發文將受處份.)|
        |\d..+?處份.?|\d..+?處份.?|\d..+?處份.?|\d.分類選填請益或心得者,進退場機制得刪去.|-+?按ctrl\+y可刪除以上內容.-+|
        |\d.+?標的:|\d.+?分類:?|\d.+?(分析/)?正文:?|\d.+?進退場機制:?.*?\n|引述.+?之銘言:|\(非長期投資者.必須有停損機制\)|
        |\d.+?警語:?|注意2:.+?\n|注意3:.+?\n|注意4:.+?\n|注意5:.+?\n|總價.+?:.*?\n|
        |已買/未買.+?\n|(\d.|\n)預算:|(\d.|\n)用途.|預算/用途:?|遊戲:|
        |CPU.+?:|MB.+?:|RAM.+?:|VGA.+?:|HDD.+?:|DVD.+?:|PSU.+?:|CHASSIS.+?:|MONITOR.+?:|SSD.+?:|
        |Mouse.+?:|其它.+?:|與估價系統.+?單號。?|禁止任何估價系統的連結.+?\n|與相關包含手寫.+?連結。?|購買套裝電腦者.+?型號。?|
        |請勿張貼.+?等|國內網路.+?敘述。?|以上只限制.+?認定。?|\d{,4}/\d{,2}/\d{,2}後會處罰。|
        |.*未買預算/用途:|注意1:自稱小妹或.*|如果您是要發問.\n|發文前.記得先按./.爬文或.z.查詢精華區.(文章類別也請更改為\[問題\])?.*\n|
        |標題中.請盡量使用關鍵字.方便(日|之)後板友查詢.\n|菜單文.+?價單號碼。|\(參閱板規.+?\)|違反者:.+?。|
        |(^RE:\s?\[\S{2}\]\s?|^RE:\s*|^\s?\[?\S{2}\]\s?))|
        |限時.+?好康|滿意寶寶.+?line.+?索取，?""", flags=re.I)


        # eyny
        self._eyny_clear = re.compile(r"""(如果瀏覽伊莉時速度太慢或無法連接(,|\s)?可以使用其他分流瀏覽伊莉(,|\\s)?www01\\.eyny\\.com.+?。|
        |若對尊貴或贊助會員有任何疑問(,|\s)?歡迎向我們查詢。我們的即時通或MSN:.*?ADMIN@EYNY\\.COM|
        |若瀏覽伊莉的時侯發生問題或不正常情況(,|\s)?請使用INTERNET EXPLORER\(I.E\)|
        |若登入不正常或變回訪客狀態(,|\s)?請先刪除COOKIE再登入。|
        |若新密碼無法使用(,|\s)?可能是數據未更新。請使用舊密碼看看。|
        |若有安裝色情守門員(,|\s)?可用無界、自由門等軟件瀏覽伊莉。或使用以下網址瀏覽伊莉:.*?HTTP://WWW\\.EYNY\\.COM:81/INDEX\\.PHP|
        |如果你覺得伊莉做得不錯(,|\s)?那就不要再猶疑了。今天就贊助和支持我們(,|\s)?立即行動!我們需要你的一點力量喔。|
        |如果發覺自己無法使用一些功能或出現問題(,|\s)?請按重新整理一次(,|\s)?並待所有網頁內容完全載入後5秒才進行操作。|
        |如果重複性登入後自動登出(,|\s)?請先刪除所有COOKIE和舊網頁再登入。|
        |升級為尊貴會員或贊助會員(,|\s)?讓你擁有自由、暢通無阻、應有盡有及所有資源任你使用的無窮快感。|
        |分享使你變得更實在(,|\s)?可以使其他人感到快樂(,|\s)?分享是我們的動力。今天就來分享你的資訊、圖片或檔案吧。|
        |上傳頭像立即獲得.*?1.*?點積分。|\[size\=.*?\]|
        |回覆中加入附件並不會使你增加積分(,|\s)?請使用主題方式發佈附件。|
        |如果你忘記伊莉的密碼(,|\s)?請在登入時按右邊出現的.*?\'找回密碼\'。輸入相關資料後送出(,|\s)?系統就會把密碼寄到你的E-MAIL。|
        |成為伊莉的版主(,|\s)?你將獲得更高級和無限的權限。把你感興趣的版面一步步地發展和豐盛(,|\s)?那種滿足感等著你來嚐嚐喔。|
        |所有積分大於負-100的壞孩子(,|\s)?將可獲得重新機會成為懲罰生(,|\s)?權限跟幼兒生一樣。|
        |伊莉影片|此帖僅作者可見|(點評)?\s?(回復)?\s?使用\s?道具\s?(評分)?\s?(檢舉)?\s?(提升卡)?|\(有影片\)|
        |下載:\s?訪客無法瀏覽下載點(\s|,)?請先\s?註冊\s?或\s?登入會員|瀏覽完整內容(\s|,)?請先\s?註冊\s?或\s?登入會員|
        |\*若看不到圖片請將|\>+?|發表於.+?前|[a-z0-9]+\s?發表於.*?(PM|AM)+|附件:.*?你需要登錄才可以下載或查看附件。沒有帳號\?註冊|
        |查看全部評分|分享\w*?收藏\w*?支持\w+?|(當前離線|當前在線).*?只看該作者|
        |\[color.*?\[\/url\]|由於本討論區.*?故不能完全監察所有留言.*?請聯絡我們.*?切勿撰寫粗言穢語.*?保留一切法律權利。|
        |提示: 該帖被管理員或版主屏蔽|(您|你)需要登錄後?才可以.+?註冊|發表回復.*?Archiver.*?Comsenz Inc\.|
        |返回列表|高級模式|重要聲明:本討論區.*?並非本網站之立場.*?法律或投資等問題\)。|回頂部|
        |本主題由.*?(合併|關閉|提升|推薦|解除限時高亮|移動)|
        |(單選|多選)投票.*?參與投票|您所在的用戶組沒有投票權限|
        |共有.*人參與投票 距結束還有:.*?分鐘|
        |-\s?.*伊莉討論區)""", flags = re.I)  


        self._eyny_space = re.compile(r"""(點評\S{,26}\s|^\S+\s發表於\s\d+-\d+-\d+\s?\d+:\d+\s?(A|P)M|\s\S+\s發表於\s\d+-\d+-\d+\s?\d+:\d+\s?(A|P)M|\s發表於\s((\d*\s+(天|小時|分鐘)|半小時)前|((前天|昨天)\s?\d+:\d+)\s?(A|P)M|\d+-\d+-\d+\s?\d+:\d+\s?(A|P)M)\S*
        |(剛剛|\d+-\d+-\d+\s?\d+:\d+\s?(A|P)M|(前天|昨天)\s?\d+:\d+\s?(A|P)M )\s?上傳下載附件\s?\([0-9.]+\s?(K|M|G)B\|
        |(【軟體(名稱|版本|語言|圖片|下載|大小|介紹)】|【系統(需求|支(持|援))】):?|
        |(【檔案(格式|大小|名稱|空間|歌詞)】|【上傳日期】|【存活時間】|【解壓密碼】|【語 {,4}言】):?|
        |(【版本資訊】|【更新日期】|【使用權限】):?|
        |\[(本|此)帖為轉載帖\]|\[此為轉載帖\]|原文刊載.+?編輯|本文轉(錄|自|載).+[a-z\u4e00-\u9fa5]|
        )""", flags=re.I)

        # ads
        self._ads = re.compile(r"""(本文由夢幻西游.*|愛文閣AIWENGE\\.COM.*|老毛桃WINPE.*|暴力貨源.*|90%打工小伙.*|成人用品情趣內衣.*|
        |有喜歡打牌的嗎\?.*|8000G教程.*|24小時咨詢熱線QQ.*|川島除濕機KAWASIMA\\.CN.*|2016年全國老板手機號碼大全.*|
        |網上購彩很方便.*|租房找辦公室來租哪儿平台.*|YAHUYULEHUI\\.COM.*|辦理銀\\*行\\*卡儲\\.蓄卡.賬\\.號卡.*|(line|賴)\s{0,3}id[a-z0-9\s]*|sent[a-z\s]*|
        |(活動詳情|報名網址).*[a-z\s]|'即日起.+?止|[a-z0-9\.]+?(jpg|jpeg).+上傳|[a-z0-9\./]+?(jpg|jpeg)|相關文章.+?編輯)""", flags=re.I)

        # 色情相關
        self._sex1 = re.compile(r"""(找.{,3}服.{,3}務[^業]|無.{0,2}套|(惡|魔).{0,2}(魔|鬼).{0,2}化.{0,2}身|(叫|找)小姐|(叫|找).{,3}小.{,3}姐|外.{0,2}送.{0,2}茶|諜對蝶|找服務[^業]|(半.{0,2}套|全.{0,2}套)服務|住家服務|小仙女|(叫|喝).{0,2}茶|外.{0,2}(約|送)|援交外約|約外服務|
        |挑美眉|約.{0,2}(妹|茶|泡)|小弟喝茶快|陰.{0,2}莖|誠信茶莊|性.{0,2}感.{0,2}秘.{0,2}書|約炮群兼.{0,2}職.{0,2}小.{0,2}姐|茶.{0,2}(姊|姐))""", flags=re.I)
        self._sex2 = re.compile(r"(LINE(:|:|\.|\+)|WECHAT|[^「](\+|加)賴|(\+|加)微信|官方*網站:|[^(連絡)]專線|SKYPE|(\+|加)瀨|】 賴|ck101\.com|ym16899|193520822)", flags=re.I)
        self._sex3 = re.compile(r"(期貨|投資能力|股票|消費者座談會|排位賽)", flags=re.I)
        self._sex4 = re.compile(r"(想要投股運順準賺!她有妙招喔|刪除的內容就像\s?DCARD\s?一樣(\s|,)?|已被\s?Dcard\s?用戶檢舉含|(違規者|違規日期|違規會員|違規會員ID|會員名稱|優良會員|優回會員):\s?.{,30}\s?|(違規事項|違規會員|違規樓層|違規帖名稱|違規網址|違規項目|優良樓層|優良網址|優回網址)|BABYHOME為您彙整站內爸媽智慧|BABYHOME集結百萬爸媽|娛樂城|免費.*裸聊|裸聊.*免費|高清.*線上看|線上看.*高清|聊天室|視訊美女)", flags=re.I)
        
        # 其他新聞類        
        self._news1 = re.compile(r"""(其他應敘明事項|基金管理人:.*基金(商品)?概況|備查文件.* |(.{2}中心|.{2}小組|記者|作者|攝影).{2,11}報導|(記者|攝影).{2,10}(∕|／|/)|(攝影.記者|攝影|記者).{3}|匯流新聞網|即時新聞.{2}報導|中評社.+?攝|(.{2}中心|.{2}小組|記者|作者|攝影).{2,11}報導|(記者|攝影).{2,10}(∕|／|/)|(攝影.記者|攝影|記者).{3}|匯流新聞網|即時新聞.{2}報導|中評社.+?攝|中時電子報|中時|工商|旺報|時報周刊|中天|時報資訊.*)""", flags=re.I)
        self._news2 = re.compile(r"((聯合報系資料照|交.{0,2}通.{0,2}新.{0,2}聞|新.{0,2}聞.{0,2}發.{0,2}布|新華社資料照|經濟日報提供|本報系資料庫|本報資料照片|報系資料照|（本報系資料庫）)|記者[^/∕::]+(/|∕|:|:)(攝影|提供)|(來源|表|製表|圖|圖表|照片|聯合報系資料照|示意圖|資料來源)(/|∕|:|:)[^/∕::]+)?\s?分享\s?FACEBOOK", flags=re.I)

        # 其他
        self._other = re.compile(r"""(\*此貨品預設為自取\s?如閣下需郵寄／順豐服務\s?請註明.*|
        |注意事項:\s?\?\?如用平郵收件\s?買家承擔平郵郵寄風險.*|^感謝客戶光顧\?\?\s?:\s?本交易號碼為\s?.*|
        |INCAR癮車報|BEAUTY美人圈|馬上\+1\s\-\s跟癮車報一同聊車.*|\sISCAR!\s|
        |\(\d+-HK\)|\(責任編輯:FINET\)|更多精彩內容.請登陸財華智庫網.*|團購好物天天下午3點開賣.{1,6}下載媽咪買APP.*|
        |BY\s?.{1,30}\s?於\s?FLICKR|良興微電影|PHOTO\sVIA[、:]+|LOOKIN|美人時髦話題網|看見女性新時尚|
        |搜更多.*相關經驗新知。\s?搜尋(,|\s)?就從BABYHOME開始。|於\d+-\d+-\d+\s?\d+:\d+:\d+\s?重新編輯過|
        |通報\s?好像有人對這篇文章有新想法唷,.*|-\s?DCARD\s?|
        |ETTODAY(新聞雲)?|-\s?[^-]+\s?-[^-]+\s?-\s?FASHIONGUIDE華人第一女性時尚美妝傳媒|
        |(第1頁|全文共.*)?文章標籤|創作者介紹.*|本帖最後由\s?.{0,26}\s?於\s?\d+-\d+-\d+\s?\d+:\d+(\s|am|pm)?編輯|
        |本帖最後由\s?.{0,30}編輯|
        |[0-9. ]+返回列表.*回頂部|
        |(^\S+|\s.{0,26})\s發表於\s\d+-\d+-\d+\s\d+:\d+|
        |(資料|文章|圖片|數據|新聞|文|訊息)?來源:(.{0,20}(研究|中心|公司|院|報|訊|網|網站|會|刊|號|政府|論壇|微信|部|局|署|報告|集團|官網|）|\))|[^ ,,。.；;]{0,20})|
        |(\d*\s+(天|小時|分鐘)|半小時)前|((前天|昨天)\s?\d+:\d+|\d+-\d+-\d+\s?\d+:\d+:\d+)\s?上傳\s?下載附件\s?\([0-9.]+\s?(K|M|G)B\\|
        |(\S+|\s.{,26})\.[A-Z]\s?\([0-9.]+\s?(K|M|G)B,\s?下載次數:\s?\d+\)\s?(下載附件\s?(保存|儲存)到相(冊|簿))?((\d+\s+(天|小時|分鐘)|半小時)前|(前天|昨天)\s?\d+:\d+|\d+-\d+-\d+\s?\d+:\d+:\d+)\s?上傳\s?(下載次數:\s?\d*)?|
        |下載附件\s{0,3}(保存|儲存)到相(冊|簿)[\sa-z0-9:-]*?\s|
        |回應\s?[^()]+\s?(\(|（)[^()]+(\)|）)\s?所寫|^.{0,20}\s?\-\s?\[|檢視相片|
        |新朋友您好(,|\s)?若需要觀賞此圖(,|\s)?請先登入或免費註冊一個帳號。|
        |.*引用自:\s?.{0,26}\s?於\s?\d+/\d+/\d+,\s?\d+:\d+|\(導護媽媽線上檢舉\).*line-height:\s?25px\">|
        |把握機會這裡買//.*|如需更多金融資訊\s?請瀏覽本行網頁:|外匯領航員遠東商銀外匯保證金交易FACEBOOK:|
        |(經濟日報提供|報系資料庫|聯合晚報提供|報系資料照片?|網路(照片|圖片)|路透|中央社|(製表|圖|照片)?(/|∕)[^/∕]{,8}(提供)?)FACEBOOK|
        |SOURCE:?\s?.{0,50}（\@.{0,30}）分享的貼文\s於\s[A-Z]+\s\d+\s年\s\d+月\s月\s\d+\s日\s\d+:\d+\s(上|下)午\s張貼\sSOURCE:?\s?.{0,30}\@INSTAGRAM|
        |(\s.{0,26})?\（\@.{0,30}\）分享的貼文\s?於\s?\d+\s?年\s?\s+月\s?月\s?\d+\s?\d+:\d+(上|下)午\s?(PDT|PST)\s?張貼\s?|
        |SOURCE:\s?\S{0,26}\s|下殺\d{0,3}\>?|
        |翻攝自?(\S{0,30}(照\s?片|畫\s?面|影\s?片|維\s?基\s?百\s?科|粉\s?絲\s?團|提\s?供|推\s?特|DCARD|微\s?博|IG|INSTAGRAM|YOUTUBE|部\s?落\s?格|粉\s?專|粉\s?絲\s?團|臉\s?書|網\s?(站|頁|路)?|報\s?紙?)|\S{0,10}\s)|
        |官網目前滿499超商取貨免運中|全館消費滿\$?\d{0,3}一律 \#超取免運|\#?(天天)?(全館)?(消費)?(限時)?滿?\$?\d{0,3}?(享|就|全)?超(商)?取(貨)?免運費?|全館消費滿499超取 運費不用錢|快揪姊姊妹妹來團購囉|
        |^DONE$|^DONE\s|\sDONE$|妳最喜愛的唐詩宋詞PART.*|延伸閱讀.*)""", flags=re.I)

        self._period = re.compile(r"。\s?FACEBOOK$", flags= re.I)

        # 卡提諾
        self._ck101 = re.compile(r"""(\*卡提諾論壇\s?CK101\.COM\s?-\s?最有梗、最用心的綜合性論壇\*|
        |【卡提諾.{2,10}】|卡提諾論壇.{0,20}→|官方.{0,20}→|趣味梗圖.{0,20}→|正妹.{0,20}→|娛樂.{0,20}→|
        |本文由.+?發表.+?謝謝!.*|可以看更多電影資訊唷↓|\[url.+?url\]|隱藏在這裡.+?\)感謝|
        |\(以下以長篇小說最為主要說明,各版版規各有不同請注意\)|本帖最後由.+?本主題由.+?於.+?分類|
        |小說區發文(內容)?事前須知.+?:(.{0,4}\d、.+(.{0,10}[a-zａｂｃｄｅｆｇｈｉ]\.\S+?){0,10}。?(.參考範例帖子.+?\　)?){0,10}|
        |延伸閱讀.*|↓↓同場加映↓↓.*|一天不看文就不對勁嗎??|點我IG傳送門一天2粒健康顧眼睛|
        |巨乳、.+?正妹日報讓你輕鬆看妹零時差|(94要|點我|想)看更多.*?(精彩圖文|精采好文|(好康|精彩)文章).*|其實關於.+?,還可以看.+?篇↓↓.*|
        |影片在(這裡↓↓↓↓↓|此)|完整影片點我|>>點我去看影片|本帖部分內容已隱藏.+?\)感謝|(\(|（)(合成)?(圖|攝影)?(/|∕|:).+?(\)|）)|
        |↓↓↓|（(翻攝自|資料照).+?）|\(中時電子報\)|＊本文章經整理分享.+?連結＊|想看更多→傳送門點|按讚.+?看更多.*|
        |上?圖?片?皆?翻攝自.+? |圖/.+? |\(翻攝.+?\)|本篇圖(文|片)來自:.+? |\((中時 |出日音樂提供)\)|
        )""", flags=re.I)

        # mobile01
        self._mobile01 = re.compile(r"""(相關食記.*|(標記)?文章出處|相關連結.*| 關注你喜愛的標籤 .*| (宜蘭好吃好玩好住|宜蘭吃喝玩樂|火鍋哪裡吃)&hearts;.*|
        |【 請繼續閱讀 】.*|版權所有.+?感謝.?!|延伸閱讀:.*|喜歡.+?的文章\?.想知道更多關於.+?的生活大小事/心情分享\?.歡迎連結到.*|
        |檢視較大的地圖|【.?飲酒勿開車.?勿騎車.?】|原文網址.?:.*?http(s)?://[a-zA-Z0-9/\.\?\=\&]+|Follow us:.*|
        |\ ?.*?記者.{0,3}∕(綜合|台北|新北)報導|※.+?的其它懶人包:.*|\&rarr\;.{0,20}\&larr\;喜歡來加入粉絲團唷!|
        |主辦單位臉書:|
        )""", flags=re.I)

        # 清除聯絡方式部分
        self._miscellaneous = re.compile(r"""(信箱|電子信箱|E-?MAIL|服務專線|客服專線|專線|客服|手機|電話|微信|WECHAT|賴|LINE(ID)?|ID|臉書|FACEBOOK|FB|YOUTUBE|IG|INSTAGRAM):\s?[-A-Za-z0-9+&@#\/%?=~_|!:,.;]+|
        |電話:?\d{2}-?\d{4}-?\d{4}\#?\d+|  
        |\d{2}/\d{4}:\d{2}|
        |(\(|（)(一|二|三|四|五|六|日)(\)|）)""", flags= re.I)

    def clean_up(self, content="", main_id = None, remove_space = True, remove_time = True):
        ''' 清除資料並返回,假如是色情文章就返回空字串
        ''' 
        self.content = str(content).lower() # 轉小寫
        self.content = self.content.strip()
        # logging.info('轉小寫: {}'.format(self.content))
        self.content = re.sub(r"^(\(|（|\[|「|『|【|〔|﹝)[^\[(（「『【〔﹝\])）」』】〕﹞]*(\)|）|\]|」|』|】|〕|﹞)", "", self.content)
        self.content = re.sub(r"(\(|（|\[|「|『|【|〔|﹝|《)[^\[(（「『【〔﹝《\])）」』】〕﹞》]*(表|攝|影片|圖|照|網|報|記者|報導|新聞|資料|授權|提供|編譯|特輯|粉絲團|粉專|直播|論壇|討論區|聊天室|部落|臉書|推特|IG|FB|INSTAGRAM|FACEBOOK|TWITTER|YOUTUBE|DCARD)[^\[(（「『【〔﹝《\])）」』】〕﹞》]*(\)|）|\]|」|』|】|〕|﹞|》)", "", self.content)
        logging.info('清括弧裡面的東西: {}'.format(self.content))
        self.content = re.sub(r"(\([0-9\.]+\)|^[\!\"#$%&’()*+,-./:;<=>?@\[\]^_`{\|}~]+)", " ", self.content)
        logging.info('清除括弧內有數字與開頭為符號: {}'.format(self.content))

        if self.is_sex():
            logging.info('偵測色情廣告或網站公告貼文')
            return ['', str(main_id)]
            # return ''
        else:      
            # self.content = re.sub('[ ]{2,10}', " ", self.content)
            # logging.info('空格清理: {}'.format(self.content))
            pass

        # 時間的開關
        if remove_time == True:
            self.content = re.sub(r"(\d+(/|-|年)\d+(/|-|月)\d*日?|((A|P)M\d{2}:\d{2}|\d{2}(/|／)\d{2})|\d{2}:\d{2}((A|P)M)?)", " ", self.content)
        else:
            pass

        self.full2half()
        logging.info('全形轉半行: {}'.format(self.content))

        self.remove_news()
        logging.info('新聞部分: {}'.format(self.content))

        self.remove_ptt()
        logging.info('ptt部分: {}'.format(self.content))

        self.remove_eney()
        logging.info('伊莉部分: {}'.format(self.content))

        self.remove_other()
        logging.info('各種網站部分: {}'.format(self.content))

        self.remove_ads()
        logging.info('廣告部分: {}'.format(self.content))

        self.remove_ck101()
        logging.info('卡提諾: {}'.format(self.content))

        self.remove_mobile01()
        logging.info('mobile 部分: {}'.format(self.content))

        self.remove_url()
        logging.info('網路連結部分: {}'.format(self.content))

        self.remove_quotation()
        logging.info('引用部分: {}'.format(self.content))
        
        self.remove_wrote_id()
        logging.info('wrote id 部分: {}'.format(self.content))

        # 清理空格開關
        if remove_space == True:
            self.remove_space()
            logging.info('空格清理: {}'.format(self.content))
        else:
            self.content = re.sub(r'[\n]+', '，', self.content)
            self.content = re.sub('[ ]+', "，", self.content)
            logging.info('空格轉逗號: {}'.format(self.content))


        self.remove_punc()
        logging.info('清除符號: {}'.format(self.content))

        self.remove_continuous_symbols()
        logging.info('連續符號部分: {}'.format(self.content))



        self.remove_contact_num()
        logging.info('聯絡方式: {}'.format(self.content))



        # 填補最後位置如果沒有符號
        if self.content != '' and self.content[-1] not in set('。，!?~'):
            self.content += '。'
        else:
            self.content

        # print(type(main_id))
        if main_id == None:
            # print('a')
            # print(main_id)
            # return [self.content, str(main_id)]
            return self.content
        else:
            return [self.content, str(main_id)]
            # return self.content
        
    def is_sex(self):
        ''' 檢查是否為色情、約泡、外送茶的訊息
        '''
        # if self._sex1.search(self.content) or self._sex2.search(self.content) or self._sex3.search(self.content) or self._sex4.search(self.content):
        #     print(self._sex1.findall(self.content), self._sex2.findall(self.content), self._sex3.findall(self.content), self._sex4.findall(self.content))
        return True if (self._sex1.search(self.content) and self._sex2.search(self.content)) or self._sex3.search(self.content) or self._sex4.search(self.content) else False

    def clean_pandas(self, _dataframe=None):
        ''' 清除 pandas to_excel 的禁用字元
        '''
        return _dataframe.applymap(lambda x: self.ILLEGAL_CHARACTERS_RE.sub(r'', str(x)) if (isinstance(x, str) and self.ILLEGAL_CHARACTERS_RE.search(x)) else str(x)) 
    
    def remove_news(self):
        ''' 其他新聞類
        '''
        self.content = self._news1.sub('', self.content) # 根據不同需求有些需要變空格有些要清除
        self.content = self._news2.sub(' ', self.content)

    def remove_ptt(self):
        ''' 清洗 ptt
        '''
        self.content = self._ptt.sub('', self.content)

    def remove_eney(self):
        ''' 清洗 伊利
        '''
        self.content = self._eyny_clear.sub('', self.content)  # 根據不同需求有些需要變空格有些要清除
        self.content = self._eyny_space.sub(' ', self.content)


    def remove_ads(self):
        ''' 廣告文
        '''
        self.content = self._ads.sub(' ', self.content)

    def remove_other(self):
        ''' 其他
        '''
        self.content = self._other.sub(' ', self.content) # 這裡雜訊都要轉換為空格以利後面做處理
        self.content = self._period.sub('。', self.content)

    def remove_ck101(self):
        ''' 卡提諾
        '''
        self.content = self._ck101.sub('', self.content)

    def remove_mobile01(self):
        ''' mobile
        '''
        self.content = self._mobile01.sub('', self.content)

    def remove_url(self):
        ''' 清除url
        '''
        self.content = self._url.sub(' ', self.content)

    def remove_quotation(self):
        '''清除引用部分
        '''
        self.content = self.quote.sub(' ', self.content)

    def full2half(self):
        '''全形字符轉半形
        '''
        n = []
        # s = s.decode('utf-8')
        s = self.content
        # s = content
        for char in s:
            num = ord(char)
            if num == 0x3000: # 漢語空格
                num = 32
            elif 0xFF01 <= num <= 0xFF5E: # 全形字符的十六进制为0xFF01 ~ 0xFF5E。
                num -= 0xfee0 # 轉半形
            symbol = chr(num)
            n.append(symbol)
        self.content = ''.join(n)
        # content = ''.join(n)

    def remove_wrote_id(self):
        '''清除 wrote 型 ID
        '''
        self.content = re.sub(r"[a-z\u4e00-\u9fa50-9_ ]{0,15}\s?wrote:", "",  self.content)
        # self.content = re.sub(r"[\u4e00-\u9fa5]{1,10}\s?wrote:", "",  self.content)

        if re.search(r"[\u4e00-\u9fa5]+\s?wrote:",  self.content):
            length = len(re.search(r"[\u4e00-\u9fa5]+\s?wrote:",  self.content).group().split('wrote:')[0])
            if 1<= length <= 10: # 拔掉wrote id 前面中文長度1-10 的情況
                 self.content = re.sub(r"[\u4e00-\u9fa5]+\s?wrote:", "",   self.content) 
            else:
                pass

        self.content = re.sub(r"\(恕刪\)", " ", self.content)
        self.content = self.content.strip()

    
    def remove_continuous_symbols(self):
        '''清除連續 ! ? 。
        '''
        # 把非小數點的 . 都變。
        new_data = ''
        for i in range(len(self.content)):

            if (i != 0 and len(self.content) -1 != i) and (bool(re.search(r'[0-9]', self.content[i-1])) and bool(re.search(r'[0-9]', self.content[i+1])) and bool(re.search(r'\.', self.content[i]))):
                new_data += '.'

            elif (i != 0 and len(self.content) -1 != i) and (bool(re.search(r'[0-9]', self.content[i-1])) and bool(re.search(r'[a-z\u4e00-\u9fa5]', self.content[i+1])) and bool(re.search(r'\.', self.content[i]))):
                new_data += '.'


            elif (i != 0 and len(self.content) -1 != i) and (bool(re.search(r'[0-9]', self.content[i-1]))  and bool(re.search(r'\%', self.content[i]))):
                new_data += '趴'


            elif (i != 0 and len(self.content) -1 != i) and (bool(re.search(r'[ ]', self.content[i-1])) and bool(re.search(r'[a-z\u4e00-\u9fa5]', self.content[i+1])) and bool(re.search(r'\:', self.content[i]))):
                new_data += ''

            elif (i != 0 and len(self.content) -1 != i) and (bool(re.search(r'[\u4e00-\u9fa5a-z0-9]', self.content[i-1])) and bool(re.search(r'[aa-z0-9]', self.content[i+1])) and bool(re.search(r'，', self.content[i]))):
                new_data += ' '  

            # 開過zipcar的福斯就不太想再租irent的toyota，福斯汽車乘坐品質比toyota好太多。
            elif (i != 0 and len(self.content) -1 != i) and (bool(re.search(r'[a-z0-9]', self.content[i-1])) and bool(re.search(r'[\u4e00-\u9fa5]', self.content[i+1])) and bool(re.search(r'，', self.content[i]))):
                new_data += ' '      
            else:
                new_data += self.content[i]

        self.content = new_data
        self.content = re.sub(r'[\!]+', '!', self.content)
        self.content = re.sub(r'[\?]+', '?', self.content)
        self.content = re.sub('[,]+', "，", self.content)
        self.content = re.sub(r'[，]+', '，', self.content)
       
        
        # self.content = re.sub(r'[\"\#\%\&\’\(\)\*\+\,\-\;\<\=\>\@\[\]\^\_\`\{\|\}\~]{2,10}', '。', self.content)
        punt = r'"#%&’()*+,-;<=>@[]^_`{|}~.'
        for i in set(punt):
            # reg = '[{}]{2,}'.format(i)
            a = """\\"""
            reg = '[' + a + i + ']' + '{2,}'
            self.content = re.sub(reg, "。", self.content)

        # clean up the unnecesary dulplicates
        self.content = re.sub(r'[\.]{2,}', '。', self.content)
        self.content = re.sub(r'[\。]+', '。', self.content)
        self.content = re.sub(r'(。，|，。)', '。', self.content)

        self.content = re.sub(r'(\?，|，\?)', '?', self.content)
        self.content = re.sub(r'(!，|，!)', '!', self.content)


        self.content = re.sub(r'~，', '~', self.content)
        self.content = re.sub(r'\?。', '?', self.content)
        self.content = re.sub(r'!。', '!', self.content)
        self.content = re.sub(r'，/，', '、', self.content)
        self.content = re.sub(r'/，', '、', self.content)



        self.content = re.sub(r"#\d{1,10};", " ", self.content)
        self.content = re.sub(r"null", " =", self.content)
        self.content = re.sub(r"&eacute;", "a", self.content)
        self.content = re.sub(r"(w{4,}|r{3,})", "。", self.content)
        self.content = re.sub(r"[\t]", '\n', self.content)
        self.content = re.sub(r'(。，|，。)', '。', self.content)
        self.content = re.sub(r'(\.，)', '.', self.content)   
        
    def remove_contact_num(self):
        self.content = self._miscellaneous.sub(' ', self.content)
        # print(self.content)


    def remove_punc(self):
        ''' 清除標點符號
        '''
        self.content = re.sub(r'[「」⊙ㄔ♂\'﹚¯∩﹝﹞∠～ˉ﹪ι口\"﹥•♀√\\∮\＋\-“#＃$＄%％&＆*＊;<>@\[\]『』^＾_＿`※《》【】●；▲★☆▼▆█◢◣◥◤▎⊿▊﹐ˊ▁▄≧ˋ≠｛｝—○”▂∴≦﹡︺︴〃∫﹙︾∞﹛∣╪﹀▇︽↙╳╞⊥╲′∴∵︵︳﹋︻︼┴﹜〕〔▃┌︰┼↓┬▅︱▍﹕→◎︹‥ⅲ↗↑╯↖×│╰÷┘┐└△├﹨─╡≒↘§▌·←＞〝‵˙°ˇ▏▕╭▉▋±－ⅰ〉〈．︶═＜≡﹒◆╮＠＝┤﹏ⅱ〒﹌■﹊□…▽–﹔]',\
        '', self.content)


    def remove_space(self):
        '''清除空格(根據不同需求來選擇清除或不)
        '''
        self.content = re.sub(r'\s+', '', self.content)
        
        # unused_words = u" \t\r\n,、。:；“‘”［］〔〕「」【】『』|=+-——─（）*&……%￥#@~·《》？/?<>,.;:'\"[]{}_)(^$`\'\"｜｛｝［］＜＞︰︴︵︶︸︹︺︼︿﹀﹋﹌﹍﹎﹏﹚﹛﹝﹞﹨＃＄％＆＊＋－．０１２３４５６７８９＝＠～"
        # for word in unused_words:
        #     self.content = self.content.replace(word, '')

        # a = re.sub(r'\n+', '\n', content)
        # a = re.sub(r'\t', ',', content)
        # print(a)
        # re.search('\t', content)

    def df_input(self, df):
        ''' 處理對象為dataframe 時以多進程方式清理
        '''
        # df = pd.read_excel("check.xlsx")
        # start = time.time()

        with concurrent.futures.ProcessPoolExecutor(max_workers = cpu_count()) as executor:
            df = self.clean_pandas(df)
            df['order_id'] = [str(i) for i in range(len(df))] # assign id
            input_dict = df[['order_id', 'content']].to_dict('records')
            futures = [executor.submit(self.clean_up, data['content'], data['order_id'], remove_space = False) for data in input_dict]
            # print('a')
            final = [future.result() for future in concurrent.futures.as_completed(futures)]
            clean_df = pd.DataFrame(final, columns = ['clean_data', 'order_id'])
            result = pd.merge(df, clean_df, how='left', on=['order_id'])
            # print(time.time() - start) 
        return result
        # return clean_df



if __name__ == '__main__':
    '''例子測試
    '''
    cn = ClearContent()
    # '''df 形式測試
    # '''
    # df = pd.read_excel("check.xlsx")
    # final = pd.DataFrame([{'content': 'kcv0319 wrote:文章這樣看下來台本真...(恕刪)哈連 頭又大 的都來湊一腳。 您怎不買一台再來說。嘖嘖嘖。都是小車有何需爭東爭西。不能換就算了。何必囉哩八嗦。有本事到台本總公司拉布條啊'}])

    # C = cn.df_input(init_df)
    # content = "(ａｐｐｌｅ６？)"
    # content = "!tony"
    # content = "winnie09855108wrote:我的時間規劃是下午一點左右開始騎,騎3、4個小時左右"
    # content = "八哥八哥八哥八哥八哥八哥wrote:看的出來是哪一排嗎（恕刪）porsche911(991.1)carrera4s"
    # content = '喔$900我我我?我3.6誠!!!...口rrrr2018年8月13日'
    # content = '#,,,!!!))???。。。[[['
    # content = "※引述《pierre0916()》之銘言:ji3ji3j3"
    # content = " tesla內裝品質還有很大進步空間看臉書社團的人抱怨不斷"
    # content = '小在昨天也上去了一趟武嶺跟跟風\n\n開著生財器具油電camry上山'
    # content = '1.需不需要上尊爵plus？'
    # content = "一分錢一分貨真要做出像honda一體成形的美觀又耐用的貨架你看看他會賣你多少2.jpg(189.71kb,下載次數:0)下載附件儲存到相簿2020-2-2911:23上傳1.jpg(186.41kb,下載次數:0)下載附件儲存到相簿2020-2-2911:23上傳"
    # content = "repsolhondateam將在2020年2月4日(臺灣時間)16:30位於印度尼西亞的雅加達舉辦車隊發表會,其發表會直播可於repsolhondateam的官方twitter收看。你。準備好marquez兄弟檔2020年賽季的新塗裝了嗎!?entrylist2.jpg(450.1kb,下載次數:0)下載附件儲存到相簿2020-2-320:19上傳<相關文章:2020賽季motogp車隊發表日>本文最後由nzj323於2020-2-320:25編輯2020,repsol,honda,marquez,honda"
    # content = "sentfromjpttonmyasusasus_x00tdb.我"
    # content = "re:[討論]有人買車純粹只是不買toyota嗎"
    # content = "https://www.mobile01.com/topicdetail.php?f=264&t=6014636"
    # content = "num00001發表於2020-2-311:17am下載:訪客無法瀏覽下載點,請先註冊或登入會員很好看,安全性十足的一部好車車室空間看起來跟hrv差不多安全棒棒!但是後座實際乘坐上,車室空間比hondahrv小很多喔!!!!...瀏覽完整內容,請先註冊或登入會員"
    # content = "本帖最後由pussycat2012於2020-2-1701:00pm編輯58311315發表於2020-2-1710:12am下載:訪客無法瀏覽下載點，請先註冊或登入會員虧在台灣的toyota賣的這麼好,好東西台灣永遠沒份,很多人靠外匯找車,再比人家貴......瀏覽完整內容，請先註冊或登入會員"
    # content = "粉紅風suzuki-伊莉討論區"
    # content = "https://electrek.co/2020/01/20/tesla-partners-baidu-map-services/●model3問世"
    # content = "hyundai科技的進步對職業運動來說,也不算件好事尤其是遇到這種不肖球隊"
    # content = "圖片1.png(26.84kb，下載次數:0)下載附件儲存到相簿 :27上傳歐規帽款常常會看到標榜符合ecer22.05為workingpartyonpassivesajaguarety(grsp)所處理圖片2.png(68.98kb，下載次數:0)下載附件儲存到相簿 :27上傳其中，ecer22代表unitednationsregulationno.22.unijaguarormprovisionsconcerningtheapprovalojaguarprotectivehelmetsandtheirvisorsjaguarordriversandpassengersojaguarmotorcyclesandmopeds簡而言之，就是規範安全帽與其配件之性能而05代表05series，也就是第5版ece22.05於 生效至今也快18年，最近則是準備迎來ece22.06那麼ece22.06相較於ece22.05有不少測試上的重大更新與新增目前預計更新新增條列如下:1.撞擊測試速度更新 如bt通訊、不允許外部突起必須為套件式設備，且組裝須由授權人員執行這樣規定將對現行的外掛產品有巨大衝擊將利於內藏式配件，或帽廠的原廠配件(但價格通常較高)ecer22.06時程 ecer22.06生效 中止ecer22.05認證 停止生產與庫存ecer22.05認證帽款 禁止銷售ecer22.05認證帽款後記:這次ece的大幅度更新，可以看到是與時俱進對應安全帽近來新發展的項目做了調整、修改如:內置片、下巴條、通訊模組。等外加部件並追加斜向撞測，與高低速撞測，更具完善而cns原本有能夠更新的機會跟上jis很可惜就這樣被葬送於歷史的角落了rejaguar:維持現狀cns23 16胎死腹中不再修訂未經同意請勿任意轉載未同意勿任意本文最後由wildspirit於 :48編輯安全帽，歐規，ecer"
    # content = "1.勞斯萊斯6.5算低調...==看到都要跪好嗎..."
    # content = "男,寅啖呵?"
    # content = "樓主借分享+1，感謝您徵求：ford.focusstlinelommel灰色車輛狀況：全新未領牌，非展示車非試乘聯絡方式：私訊直接報價，勿丟名片交易地區：北部請私訊報1.空車最大折價（貸款）2.贈送配件項目(型號規格)/交車禮3.交車日期4.其他規費，明細5.菜單內容越詳細越好"
    # re.sub("-\s?.?\s?伊莉討論區", '', content)
    # re.sub("-\s?.*伊莉討論區", '', content)
    # content = ' 藍寶堅尼Aventador LP700-4 這台也問9年了 還是很新潮\n\nttt'
    # content = '車是負資產，跟房子不同，雖說每個人的現金運用方式不同，但還是建議至少當下要有足夠的現金存款能一次買下整台，再來考慮貸款購車，否則要是後續有突發裝況，那貸款會很有壓力您現在手頭現金不夠覺得有負擔，那就該考慮便宜一點的車子。喜歡進口車的話，不妨考慮 RAV4/ Tiguan / Kodiaq 都是空間不錯又便宜一點的選擇。'
    # content = '個人覺得旅行車賣不好的原因是因為日系車沒有多的旅行車可選，反而歐系車比較多，大家重視的還是妥善率排第一(有人錢就不包括在內)，如果honda/Toyata/Nissan出個國產旅行車，價錢合理，一定有固定的銷售量!ps:skoda superb combi當初超想買，最後妥善率讓我不敢下手'
    # content = '如果honda/Toyata/Nissan出個國產旅行車，價錢合理，一定有固定的銷售量!ps:skoda superb combi當初超想買，最後妥善率讓我不敢下手'
    # content = 'wagon系列我覺得skoda比vw好看很多。'
    # content = '只有我覺得這串是反串嗎ford和vw兩者比妥善率(黑人問號)?然後又是各種萬年dsg老調。是在哈囉。'
    # content = 'ic-2430540 wrote:真的不知道他回什麼？...(恕刪)可以請RAV。，4一起來玩沙啊!。笑他不敢……'
    # content = '100萬左右就直上 rav4，爸媽坐後座也較舒適'
    content = '2.3 我。。。'
    print(cn.clean_up(content , remove_space = False, remove_time = False))
    del content
    gc.collect()


    # with pd.ExcelWriter('after_clean1.xlsx', options={'strings_to_urls': False}) as writer:
    #     result.to_excel(writer)

 



    # import gc
    # gc.collect()
    # test_list = [[1,2],[3,4], 'tony']
    # func = lambda x: [y for t in x for y in func(t)] if type(x) is list else [x]
    # func(test_list)
    # a = [2,4,6]
    # b = lambda x: [i*6 if i>4 else i for i in x  ]
    # b(a)
    # func = lambda x: [y for t in x for y in func(t)] 
