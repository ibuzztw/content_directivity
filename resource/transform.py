import pandas as pd


df_brand = pd.read_excel('汽車名稱辭庫.xlsx', sheet_name= '品牌').astype(str)

# each_set_brand = df_brand.iloc[:,1:].values
# each_set_brand = ['+'.join(list(filter(lambda x: x != 'nan', i))) for i in each_set_brand]
# new_df_brand = pd.DataFrame(each_set_brand, columns= ['品牌名稱'])
df_product = pd.read_excel('汽車名稱辭庫.xlsx', usecols= ['品牌名稱', '產品名稱'], sheet_name= '產品').astype(str)
pd.merge(new_df_brand, df_product, how="left", on = new_df_brand['品牌名稱'] )


new_df_brand.join(df_product, on= '品牌名稱', lsuffix='_caller', rsuffix='_other')


check_df = pd.merge(df_brand, df_product, how="left", on = '品牌名稱' , )

combine_df = check_df.iloc[:,1: -1].values
combine_df = ['+'.join(list(filter(lambda x: x != 'nan', i))) for i in combine_df]

product_name = check_df.iloc[:, -1].values.tolist()

final_df = pd.DataFrame(list(zip(combine_df, product_name)), columns= ['品牌名稱', '產品名稱'])

with pd.ExcelWriter('check.xlsx') as writer:
    final_df.to_excel(writer)