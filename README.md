# 程式說明
# Branches
* master: 現版本

若要修改請新開 branch
# 指向性切割

## 功能說明
此專案主要目的是以規則法搭配關鍵字把文章做切割

### 使用者說明
使用者先呼叫 ContentDirectivity class，接著輸入想探討的產業。再來，會接受兩種方式導入資料並產出結果
1. call cd = ContentDirectivity('汽車') --> cd.pipeline('benz比bmw 好很多。' --> output
2. call cd = ContentDirectivity('汽車') --> input_df = cd.get_input_df('2020-05-08', '2020-05-09') (這是時間區段參數) --> cd.get_output_df(input_df) --> output以df 形式



## 開發者說明
### ContentDirectivity class 
最後呼叫的class 與產出的結果都在 ContentDirectivity.py 改而之後要客製化輸出也是在這
- get_input_df 輸入df 的形式
- get_output_df 輸出df 的形式
- pipeline 輸入文章並產出切割後的短文

### Preprocessor class
所有預處理的過程都在這裡進行，裡面函數定義為以下
- get_channl_ids 得到特定產業裡所有相關的 ids
- get_pro 載入所有相關pro 的資料
- get_keyword 載入所有名稱詞彙的資料
- get_and_clean_data 得到並清理資料
- extract_subjects 萃取主題關鍵字
- remove_empty_subjects 把經過回朔到title 主題依然沒有主題的回文或內文刪除
- call_api 連接拉資料api

### Segment class
所有主要的切個規則與邏輯都在這
- get_query 把關鍵字轉成正則語法來查找資料
- check_bool 查找文章是否符合該規則並返回 bool
- cut_by_comma 用逗號切割
- first_step_cut 用 。?!~ 符號切割
- second_step_cut 用規則法搭配關鍵字做切割與擷取
- unify_link_symbol 處理連接語句，把品牌與品牌間的連接詞統一變、
- connect_sentence 組併句子
- assign_type 給予句子種類 e.g. 比較句




## Usage
```python
import re # 正則法搜尋關鍵字並做取代的動作

import pandas # 處理對象如為dataframe 形式

import concurrent.futures # 對象為dataframe 用多進程來加快程式執行的速度

from multiprocessing import Process, Pool, cpu_count # 其他執行多現成需要的packages

import logging # 紀錄程式跑的過程

import jieba # 斷詞用

import requests # 接api
```

