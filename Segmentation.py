import sys
import os
import re
from re import search, finditer, sub, findall
import pandas as pd

import logging 
import jieba

import gc

logger = logging.getLogger("指向性切割")
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s : %(message)s')

'''
指向性切割
'''
class Segment(object):
    def __init__(self, doc, keyword):
        if isinstance(doc, str): 
            self.doc = doc.lower() # 如果是字串 英文全部轉小寫

        self.keyword = set(keyword) 
        self.query, self.new_query = self.get_query()

        # 回憶
        self.recall_re1 = re.compile('.{0,20}?' + self.query +'.{0,20}?'+'(原本|曾經|曾用|之前|過去|也曾)'+ '.{0,20}' + self.query)

        # 譬喻 設定主題...跟的距離.{0,30}?
        self.alike_re1 = re.compile('[^，]{0,10}?' + self.query +'[^不|沒]{0,25}?'+ '(像|好像|很像|有點像|太像|就像|像是|類似|相似|比得上|等於)' + '[^但]{0,10}?' + self.query + '.*?(。|，|!)')
        self.alike_re2 = re.compile('[^，]{0,20}?' + self.query + '[^不|沒]{0,30}?' + '(跟|和|與|、)' + '.{0,10}?' + self.query + '[^但]{0,20}?' + '(好像|很像|有點像|太像|就像|像是|類似|相似|一樣|差不多|相同|同等|最好)' + '.{0,30}(~|。|，|!)')
        self.alike_re3  = re.compile('[^。]{0,5}' + self.query + '.{0,10}' + '、.{0,10}' + self.query + '[^。]{0,25}?' + '(都是|全都|全部|都|是|等|.)'  + '.{0,40}(~|。|，|!)' )
        self.alike_re4 = re.compile('[^，]{0,20}?' + self.query + '[^不|沒]{0,20}?' + '(有|繼承|具有)' + '[^但]{0,10}?' + self.query + '.{0,10}?' + '(性能|特性|特點|特色|特徵|元素|機能|功能)' + '.*?(，|。|!|~)')
        
        # 比較 (加了前綴因素) 改.{0,30}?  .{0,40}?    後綴描述.{0,40}(~|。|，|!|\?)
        self.re1_split = '(比(?!如|得上|較)|比起|相較|相比|不輸|勝過|贏過|輸給|超越|超過|打趴|敗給|比不上|不如|遠輸|較大|較小|較好|能贏|屌打|贏|大勝|優於|會低|輸|勝)'
        self.compare_re1 = re.compile('[^，]*?' + self.query +'[^耶]{0,30}?' + self.re1_split + '[^但]{0,10}?' + self.query + '.{0,35}(~|。|，|!)')
        self.re2_split = '(跟|和|與|相對|沒)'
        self.compare_re2 = re.compile('[^，]{0,20}?' + self.query + '.*?' + self.re2_split + '.{0,10}?' + self.query + '[^但]{0,25}?' + '(比|比較|比起|相較|相比|不輸|勝過|贏過|輸給|超越|比起來|比不上|不如|遠輸|較大|較小|較好|差很多|高)' + '.{0,30}(~|。|，|!)')
        self.re3_split = '(像|類似|比得上)'
        self.compare_re3 = re.compile('[^，]*?' + self.query +'.{0,30}?' + '(不|沒).{0,5}?' + self.re3_split + '[^但]{0,10}?' + self.query + '.{0,35}(~|。|，|!|\?)')
        self.re4_split = '(被|把)'
        self.compare_re4 = re.compile(self.query + '.{0,15}' + self.re4_split + '[^但]{0,3}' + self.query + '.{0,3}' + '(勝過|贏過|輸給|超越|超越|遠輸|屌打|輾壓|贏|大勝|輸|甩開)'+ '.{0,20}(~|。|，|!)')

    def get_query(self):
        # 當資料庫拉資料出來,並記錄的關鍵字
        # 把記錄下來的keywords, 轉換成正則形式進行查詢
        keyword = list(set(self.keyword))
        new_keyword = keyword.copy()
        query = '(' + '|'.join(keyword) + ')'
        
        for i in range(len(keyword)):
            if new_keyword[i].encode('utf-8').isalpha():
                new_keyword[i] = f'[^a-z]{new_keyword[i]}[^a-z]|^{new_keyword[i]}|{new_keyword[i]}$'
            else:
                pass
        new_query = '(' + '|'.join(new_keyword) + ')'
        return query, new_query


    def check_bool(self, re_compile, content):
        if re_compile.search(content):
            return True
        return False

    def cut_by_comma(self, docu):
        # docu = "我我五samsung表現還不錯。。。但是apple的cp值也很好，而我sony也是可以值得考慮的。"
        sentences = re.split(r"([，])", docu)
        sentences.append("")
        sentences = ["".join(i) for i in zip(sentences[0::2], sentences[1::2])]

        return sentences


    def first_step_cut(self, docu):
        if len(self.keyword) > 1:
            sentences = re.split(r"([。?!~])", docu)
            sentences.append("")
            sentences = ["".join(i) for i in zip(sentences[0::2], sentences[1::2])]
        else:
            sentences = [docu]

        #dealing with ellipsis e.g. 只是屏幕有点小。。。 or...
        new_sentence = []
        for i in range(len(sentences)):   
            sentences[i] = sentences[i].strip()
            if len(sentences[i]) <= 200:
                if (not (search(r'^.{1}$',sentences[i])) and not search(r'^[A-Za-z]$',sentences[i]) and \
                    not search(r'[\n]', sentences[i])) and not '' == sentences[i]:
                    new_sentence.append(sentences[i])
            else:
                new_sentence.append('')
                logging.info("文章字字源長度過長: {} 而Objects 有 {}".format(sentences[i], self.keyword))
        logging.info("第一次拆句後:{} 而 keys {}".format(new_sentence, self.keyword))
    
        return new_sentence


    def second_step_cut(self):
        # 檢視每一個 。 \n 符號拆開的短文
        inputt = self.first_step_cut(self.doc)  

        listt = []
        dicc = {}
        for i in range(len(inputt)):
            alike_text = self.unify_link_symbol(inputt[i])
            obj = set(findall(self.query, inputt[i]))

            alike_bool = [self.check_bool(self.alike_re1, inputt[i]), self.check_bool(self.alike_re2, inputt[i]), self.check_bool(self.alike_re3 , alike_text), self.check_bool(self.alike_re4, inputt[i])]
            compare_bool = [self.check_bool(self.compare_re1, inputt[i]), self.check_bool(self.compare_re2, inputt[i]), self.check_bool(self.compare_re3, inputt[i]), self.check_bool(self.compare_re4, inputt[i])]
            all_bool = alike_bool + compare_bool

            [jieba.add_word(k) for k in obj]
            start = 0
            if inputt[i] == '':
                listt.append('')

            # 如果一篇短文出現單一主題或是無主題, 就直接斷詞不進行切割
            elif len(obj) <= 1:
                listt.append(inputt[i])
                logging.info('單一主題短文')    

            # 多品牌但品牌間不含有複數指向的因素 
            elif len(obj) >= 2 and not self.recall_re1.search(inputt[i]) and all([a == False for a in all_bool]):
                a = self.cut_by_comma(inputt[i]) 
                logging.info('第二次拆句: {}'.format(a))
                for k in range(len(a)):
                    ob2 = findall(self.query, a[k])

                    if len(ob2) <= 1:
                        listt.append(a[k])
                        logger.info('單一主題或無主題') 

                    elif len(ob2) >= 2:
                        # [jieba.add_word(word) for word in ob2]
                        seg_list = jieba.lcut(a[k], cut_all=False) 
                        # print(seg_list)
                        obj_pos = [o for o in range(len(seg_list)) if re.search('|'.join(ob2), seg_list[o])]
                        # print(obj_pos)
                        distances = [obj_pos[i] - obj_pos[i-1] for i in range(len(obj_pos)) if i != 0]
                        # print(distances)
                        new_obj_pos = obj_pos + [len(seg_list)]
                        # print(new_obj_pos)    
                        new_distances = [new_obj_pos[i] - obj_pos[i-1] for i in range(len(new_obj_pos)) if i != 0]
                        # print(new_distances)

                        string = ''
                        if all([d >= 5 for d in distances]):
                            for j in range(len(seg_list)):
                                # if seg_list[j] in ob2 and j != 0: # 防止主題在第一位變空值
                                if re.search('|'.join(ob2), seg_list[j]) and j != 0:
                                    token = seg_list[start: j]
                                    start = j
                                    listt.append(''.join(token))

                                elif len(seg_list) - 1 == j:
                                    token = seg_list[start: j + 1]
                                    start = j
                                    listt.append(''.join(token))
                        
                        elif any([d >= 5 for d in new_distances]): # 只要偵測到任何一個主體間的距離大於 5 就執行
                            for object_index in range(len(new_obj_pos)):
                                if object_index == 0 and new_distances[object_index] < 5:
                                    seg_output = ''.join(seg_list[new_obj_pos[object_index]: new_obj_pos[object_index] + new_distances[object_index]])
                                    string += seg_output
                                    
                                elif object_index != len(new_obj_pos) -1 and new_distances[object_index] >= 5:
                                    seg_output = ''.join(seg_list[new_obj_pos[object_index]: new_obj_pos[object_index] + new_distances[object_index]])
                                    string += seg_output
                                    listt.append(string)
                                    string = '' # 重設string

                                elif object_index != len(new_obj_pos) -1 and new_distances[object_index] < 5:                        
                                    seg_output = ''.join(seg_list[new_obj_pos[object_index]: new_obj_pos[object_index] + new_distances[object_index]])                               
                                    string += seg_output

                                elif object_index == len(new_obj_pos) -1:                        
                                    listt.append(string)
                        else:
                            listt.append(''.join(seg_list))

                        logger.info('多主題不含有複數指向')  

            # 多品牌但主題間含有複數指向的因素
            # 處理回憶詞
            elif len(obj) >= 2 and self.recall_re1.search(inputt[i]) and all([a == False for a in all_bool]):
                menmory_keys = set(['原本', '曾經', '曾用', '之前', '過去', '也曾'])
                a = self.cut_by_comma(inputt[i]) # 分開關鍵字跟關鍵字間如有 '，'
                logging.info('第二次拆句: {}'.format(a))
                for k in range(len(a)):
                    ob2 = findall(self.query, a[k])
                    if  len(ob2) <= 1:                    
                        listt.append(a[k])

                    elif len(ob2) >= 2:
                        seg_list = jieba.lcut(a[k], cut_all=False) 
                        for j in range(len(seg_list)):
                            if seg_list[j] in menmory_keys:
                                token = seg_list[start: j]
                                start = j
                                listt.append(''.join(token))

                            elif len(seg_list) - 1 == j:
                                token = seg_list[start: j + 1]
                                start = j
                                listt.append(''.join(token))

                logger.info('多主題含有複數指向(回憶詞)')  

            # 處理譬喻詞跟比較詞 (擷取精華語句)
            elif len(obj) >= 2 and any([b == True for b in all_bool]):
                if any([a == True for a in alike_bool]) and any([c == True for c in compare_bool]):
                    extract_alike = list(self.alike_re1.finditer(inputt[i])) + list(self.alike_re2.finditer(inputt[i])) + list(self.alike_re3.finditer(alike_text)) + list(self.alike_re4.finditer(inputt[i]))
                    re_match = [self.compare_re1.search(inputt[i]), self.compare_re2.search(inputt[i]), self.compare_re3.search(inputt[i]), self.compare_re4.search(inputt[i])]
                    res = [self.re1_split, self.re2_split, self.re3_split, self.re4_split]

                    for i in range(len(re_match)):
                        dicc = {}
                        obj_list = [] 
                        if re_match[i]:
                            split_result = re.split(res[i], re_match[i].group())
                            for j in split_result:
                                ori = set(findall(self.new_query , j))
                                new = set(map(lambda b: ''.join(re.findall('[a-z]+', b)) if re.search('[\u4e00-\u9fa5]*[a-z]+[a-z][\u4e00-\u9fa5]*', b) else b, ori))
                                obj_list.append(new)
                            dicc.update({ 'type': '比較語句', 'content': re_match[i].group(), 'content_subjects':  obj_list})
                            listt.append(dicc)

                    # 一個句子含有比較與譬喻特性 譬喻部分不會再執行
                    # e.g A、B 都來的不錯 (譬喻) vs A、B 比 C 都來的不錯 (比較) 這裡會只抓比較語句
                    if not list(self.alike_re3 .finditer(alike_text)):
                        for j in extract_alike:
                            dicc = {}
                            dicc.update({'type': '譬喻語句', 'content': j.group(), 'content_subjects': set(findall(self.new_query , j.group()))})
                            listt.append(dicc)
                        logger.info('多主題含有複數指向(譬喻詞)')
                    else:
                        pass

                elif any([a == True for a in alike_bool]):  
                    if list(self.alike_re2.finditer(inputt[i])) != []:
                        extract_alike = list(self.alike_re1.finditer(inputt[i])) + list(self.alike_re2.finditer(inputt[i])) + list(self.alike_re4.finditer(inputt[i]))
                        for j in extract_alike:
                            dicc = {}
                            dicc.update({'type': '譬喻語句', 'content': j.group(), 'content_subjects':  set(findall(self.new_query , j.group()))})
                            listt.append(dicc)
                        logger.info('多主題含有複數指向(譬喻詞)')
                        
                    else:
                        extract_alike = list(self.alike_re1.finditer(inputt[i])) + list(self.alike_re3.finditer(alike_text)) + list(self.alike_re4.finditer(inputt[i]))
                        for j in extract_alike:
                            dicc = {}
                            dicc.update({'type': '譬喻語句', 'content': j.group(), 'content_subjects':  set(findall(self.new_query , j.group()))})
                            listt.append(dicc)
                        logger.info('多主題含有複數指向(譬喻詞)')

                elif any([c == True for c in compare_bool]):
                    re_match = [self.compare_re1.search(inputt[i]), self.compare_re2.search(inputt[i]), self.compare_re3.search(inputt[i]), self.compare_re4.search(inputt[i])]
                    # print(re_match)
                    res = [self.re1_split, self.re2_split, self.re3_split, self.re4_split]
                    for i in range(len(re_match)):
                        dicc = {}
                        obj_list = [] 
                        if re_match[i]:
                            split_result = re.split(res[i], re_match[i].group())
                            for j in split_result:
                                ori = set(findall(self.new_query , j))
                                new = set(map(lambda b: ''.join(re.findall('[a-z]+', b)) if re.search('[\u4e00-\u9fa5]*[a-z]+[a-z][\u4e00-\u9fa5]*', b) else b, ori ))
                                obj_list.append(new)

                            dicc.update({ 'type': '比較語句', 'content': re_match[i].group(), 'content_subjects':  obj_list})
                            listt.append(dicc)
        # print(listt)

        connected_sentence = self.connect_sentence(listt)
        
        type_assigned = self.assign_type(connected_sentence)
        return type_assigned


    def unify_link_symbol(self, docu):
        # make sure at least 2 keyword was detected
        # docu = '感覺很cp現在再選可能真的不考慮mazda會選ford366或toyota了。'
        # self.query ='(好奇|日本大王|愛快|honda|benz|bmw|mazda|ford|toyota|nissan|apple|samsung)'
        link_symbols = '(\.|,|\+|\/|∕|╱|\(|\)|=|&|和|跟|與|及|以及|或是|或者|或|vs|and| )'
        if self.query != '':
            match_cases = list(finditer(self.query + '\s?.{0,3}' + link_symbols + '\s?.{0,3}' + self.query, docu)) 
            # print(match_cases)
            for i in range(len(match_cases) * 2):
                if search(self.query + link_symbols + self.query, docu):
                    a = search(self.query + link_symbols + self.query, docu).group()
                    split_point = search(link_symbols, a).group()
                    final = a.split(split_point)
                    docu = sub(final[0] + link_symbols + final[1], final[0] + '、' + final[1], docu)

                # 處理主題與主題和連接詞中間有空格的時候
                elif search(self.query + '\s?' + link_symbols + '\s?' + self.query, docu):
                    a = search(self.query + '\s?' + link_symbols + '\s?' + self.query, docu).group()
                    # a = sub('\s', '', a)
                    # print('aaa')
                    split_point = search(link_symbols, a).group()
                    # print(split_point)
                    final = a.split(split_point)
                    # print(final)
                    docu = sub(final[0] + '\s?'+ link_symbols + '\s?' + final[1], final[0] + '、' + final[1], docu)
                    
                # 處理主題與主題和連接詞中間有空格的時候
                elif search(self.query + '\s?.{0,4}' + link_symbols + '\s?.{0,4}' + self.query, docu):
                    a = search(self.query + '\s?.{0,4}' + link_symbols + '\s?.{0,4}' + self.query, docu).group()
                    split_point = search(link_symbols, a).group()
                    final = a.split(split_point)
                    docu = sub(final[0] + '\s?.{0,4}' + link_symbols + '\s?.{0,4}' + final[1], final[0] + '、' + final[1], docu)

                else:
                    # return 
                    break
        else:
            # pass
            return docu

        docu = sub('[/∕╱=]', '', docu)
        return docu
        

    def connect_sentence(self, list_of_sentence):
        final = []
        for i in range(len(list_of_sentence)):
            if isinstance(list_of_sentence[i], str):
                final.append(list_of_sentence[i])
            elif isinstance(list_of_sentence[i], dict):
                final = list_of_sentence
                break

        logger.info('接句前: {}'.format(final))

        del list_of_sentence

        combined_sentence = []
        string = ''
        if all([isinstance(k, dict) for k in final]): # 判斷是否為譬喻或比較語句
            combined_sentence = final  
        else:
            if len(final) >= 2:
                for i in range(len(final)):
                    # 此為比較與譬喻
                    if isinstance(final[i], dict):
                        # 此為比較譬喻句前句為一般句子
                        if isinstance(final[i-1], str) and len(string) != 0:
                            combined_sentence.append(string)
                        combined_sentence.append(final[i])

                    # 此處開始為一般情況
                    elif  i == 0 and final[i] != '':
                        string += final[i]
                        
                    elif i == 0 and final[i] == '':
                        combined_sentence.append(final[i])
                
                    elif (i != 0  and '。' not in final[i-1]): # 前文不含有結尾意涵
                        if (findall(self.query, final[i]) ==  findall(self.query, string)): # 前句跟後句同主題合併                       
                            string += final[i]
                            if i == len(final) -1:
                                combined_sentence.append(string)

                        elif not search(self.query, final[i]) and search(self.query, string): # 前文有主題後文無主題合併
                            string += final[i] 
                            if i == len(final) -1:
                                combined_sentence.append(string)

                        elif findall(self.query, final[i]) != findall(self.query, string): # 前句跟後句不同主題分開
                            combined_sentence.append(string)
                            string = ''
                            string += final[i]
                            if i == len(final) -1:
                                combined_sentence.append(string)
                        else:
                            combined_sentence.append(final[i])

                    elif i != 0 and '。'  in final[i-1]: # 前文含有結尾意涵
                        combined_sentence.append(string)
                        string = ''
                        string += final[i]
                        if i == len(final) -1:
                            combined_sentence.append(string)

                    elif (i != 0  and '、'  in final[i-1]): # ford、馬三、toyota都還是有空間不足的問題，
                        string += final[i]
                        if i == len(final) -1:
                            combined_sentence.append(string)    
            else:
                combined_sentence = final
        del final
        logger.info('連接句子: {}'.format(combined_sentence))

        return combined_sentence

    def assign_type(self, combined_sentence):
        # print(combined_sentence)
        assigned_result = []  
        if combined_sentence == []:
            dicc = {}
            dicc.update({'type': '文章字字源長度過長', 'content': ''})
            assigned_result.append(dicc)

        elif all([isinstance(i, dict) for i in combined_sentence]):
            assigned_result = combined_sentence
        
        else:
            for i in range(len(combined_sentence)):
                if isinstance(combined_sentence[i], str):
                    check_list = [j for j in self.keyword if j in combined_sentence[i]]

                    if combined_sentence[i] == '':
                        dicc ={}
                        dicc.update({'type': '文章字字源長度過長', 'content': '', 'content_subjects':  ''})
                        assigned_result.append(dicc)

                    elif len(check_list) == 0 and len(combined_sentence[i]) <= 150:
                        dicc ={}
                        dicc.update({'type': '不含主題', 'content': '', 'content_subjects':  ''})
                        assigned_result.append(dicc) 

                    elif len(check_list) == 1 and len(combined_sentence[i]) <= 150:
                        # print(self.new_query)
                        # print(set(findall(self.new_query , combined_sentence[i])))
                        dicc = {}
                        dicc.update({'type': '單一主題', 'content': combined_sentence[i], 'content_subjects':  set(findall(self.new_query , combined_sentence[i]))})

                        assigned_result.append(dicc)

                    elif len(check_list)  > 1 and len(combined_sentence[i]) <= 150:
                        dicc = {}
                        dicc.update({'type': '複數主題且無複數指向', 'content': combined_sentence[i], 'content_subjects': set(findall(self.new_query , combined_sentence[i]))})
                        assigned_result.append(dicc)

                    else:
                        dicc = {}
                        dicc.update({'type': '無主題或文章長度太長', 'content': '', 'content_subjects':  ''})
                        assigned_result.append(dicc)

                elif isinstance(combined_sentence[i], dict):
                    assigned_result.append(combined_sentence[i])

        for i in range(len(assigned_result)):
            if not isinstance(assigned_result[i]['content_subjects'], list):
                assigned_result[i]['content'] = re.sub(' ', '', assigned_result[i]['content'])
                assigned_result[i]['content_subjects'] = set(map(lambda b: ''.join(re.findall('[a-z]+', b)) if re.search('[\u4e00-\u9fa5]*[a-z]+[a-z][\u4e00-\u9fa5]*', b) and isinstance(b, str) else b,  assigned_result[i]['content_subjects'] ))
            else:
                pass
        return assigned_result
        

if __name__ == '__main__':    
    ### 單一主題
    # docu = '百優實在是太好用了。他們家的東西我都喜歡，這次的新品也好生火。'
    # docu = '感覺中華汽車cross還可以再塞不少耶，'
   
    ### 複數主題但不含有複數指向的因素
    # docu = 'samsung表現還不錯但是真的apple的cp值也很好，而sony也是可以值得考慮的。samsung表現還不錯但真的不錯。表現還不錯但真的不錯'
    # docu = '版友們說的好用物有些用過卻都沒感覺，例如百優，感覺擦了後只是有基本的保濕，但其它的效果都沒有如抗老，蘭寇的那瓶擦黑眼圈及細紋的有冰珠的精華液也是，擦了等於沒擦'
    # docu = '我我benz我五，BMW變速箱調教熱血，感受的到拉轉換檔的頓點，且benz直6引擎高轉聲浪棒，E60懸吊硬，底盤貼地，綜合這3點，感官會覺得323快，同等級的車'
    # docu = '我個人會想要APPLE的，因為我之前都是用HTC，可是最近用了一下弟弟的apple後，覺得apple好像比較適合我，就是價錢真的貴阿。'

    ### 複數主題含有複數指向的因素
    # docu = 'naras出奇的好用跟我之前的保養品雅詩蘭黛平分秋色。'
    # docu = '逛街看到活動在介紹這款腮紅試在手背覺得不錯看就停留繼續看老師示範創新粉霧慕絲質地輕盈又柔軟，色彩生動持久，可塑性極高，可於頰部暈染、疊擦、玩色。超越妳對頰彩的一般想像，全新摩霧頰彩慕斯，慕絲狀的質地，推抹開卻是輕盈的粉末，獨特的柔暈光，輕柔的粉霧色澤，輕拍，卻能超有感的實現長達8小時前所未見的時尚好氣色，可以玩色，更能夠混搭疊擦，輕盈如空氣般的霧氣質地，零妝感的讓肌膚會呼吸、讓腮紅會呼吸！8個時尚有感慕斯設計，更是以資生堂品牌的謬思女神，與創業至今的重要合作對象為靈感，致敬每一位時尚且獨一無二的代表！(以上為官網產品說明)正方形玻璃罐裝黑色旋轉罐蓋試用的色號是06佐代子內為凝凍狀偏軟嫩Q彈的膚觸凝凍狀很好推開不會有色塊與NARS炫色腮紅露一樣都帶有大量很細碎精緻的。naras新出的裸光奇肌粉底液很滿意雅詩，之前的保養品是用專櫃(雅詩蘭黛媽媽給的好用！！)，出社會後第一次自己購入保養品是倩碧，第一組彩妝就獻給naras了！！金色微粉偏暖粉橘紅珠光感輕沾一點推開很好上色很自然也不會變猴子屁股櫃姐還用刷具推開變漸層眼影也很顯色優:好推勻/顯色/自然/珠光感'
    # docu = 'naras新出的裸光奇肌粉底液很滿意之前的保養品是用專櫃(雅詩蘭黛媽媽給的好用，出社會後第一組彩妝就獻給naras了，'
    # docu = '代號g05的全新第四代bmwx5於2018年10月於巴黎車展全球首演，隨後總代理汎德即火速在12月將其導入國內販售。現在 x5又新增了入門的25d柴油動力，也讓車系的入手門檻下壓至300萬元之內。話雖如此 x525d的豪華配備並未減損太多，就像今日試駕的「xdrive25d旗艦版」，外觀同樣有帥氣的xline外觀套件妝點，品牌新世代的personalcopilot智慧駕駛輔助系統、全數位虛擬座艙、全套智慧駕駛等科技配備也一應俱全，g05x5在去年12上市時，導入包括40i、30d、m50d等三種動力在內的五款車型，而入門的xdrive30d豪華版則要價343萬元；2019年8月總代理規劃2020年式新車型時，順勢為x5追加了25d動力以取代原有的30d 並藉由入門的「25dxdrive豪華版」將車系入手門檻壓在與f世代x5相差不多的289萬元，至於本次試駕的「xdrive25d旗艦版」則跨過300萬元大關來到305萬元。bmwx5車系編成與價格表車型xdrive25d豪華版xdrive25d旗艦版xdrive40i豪華版xdrive40i旗艦版m50d價格289萬元305萬元345萬元373萬元535萬元相較入門的25d豪華版，旗艦版除了內裝多出sensatec皮質包覆外，外觀則加入了xline套件，包括施以銀色塗裝的前後下護板以及專屬鋁圈，至於水箱護罩、前進氣壩飾條、車側飾條與飾板、窗框、車頂架、排氣尾管等處也皆運用緞面鋁質鋪陳讓外觀看來更為粗獷；當然，若喜歡運動氛圍的車主也可選配msport套件。就像其他新世代車款一樣 bmw為邁入g世代的x5搭配了面積放大不少的水箱護罩，當然也具備主動式進氣調節功能；藉由鋁質、霧銀、霧黑等不同顏色與材質的搭配，為車頭營造出多層次又豐富的視覺效果。25d與40i等車型皆標配led頭燈組，車主當然也可選擇僅有m50d車型才標配、含glare-free光型變化功能的智慧led頭燈（4、6萬元），甚至是內部具備湛藍色「x」燈組的雷射頭燈（13、2萬元）。鋁質登車踏板不僅可增添外觀的粗獷感，對於家中有小朋友的車主而言更是項實用配備。車尾同樣具備xline套件專屬的霧銀色下護板，並與其他高階車型一樣運用方形鍍鉻尾飾管增添些許性能風格。尾燈-1尾燈-2尾燈當然也用上了品牌最新的家族化設計，以更簡潔的多層次輪廓營造出立體感。19吋鋁圈也是xline套件的專屬配備，並搭配265、50r19規格的失壓續跑胎。g11、g127series首見的迎賓光毯設計在廣受好評之後，目前也成為了x5全車系的標準配備，解鎖車輛或開門時即會將「天使之翼」圖樣投影至地面迎接車主，在光線昏暗處上車時真是讓人倍感尊榮，藉由全新設計的家族化線條，讓現行x5的內裝與前代車型相較可說煥然一新 idrive7、0系統與數位儀錶則是進化重點之一，讓車室看來更豪華也更具科技感；此外xdrive25d旗艦版與其他高階車型一樣，在整個中控台運用sensatec皮質包覆。中控台以鋁質條紋飾板鋪陳是25d與其他車型於內裝上的一大差異，但卻也因此使其多了些冷冽的科技感，下方則同樣搭配了環艙氣氛燈。環艙氣氛燈-1環艙氣氛燈-2環艙氣氛燈-3除了中控台飾板下方外，包括中央鞍座、車門板等處也設計了環艙氣氛燈，在夜間為車室營造出十分特別的氛圍。門板與中控台一樣帶有豐富層次感，並以座椅同色皮革與鋁質條紋飾板點綴。方向盤-1方向盤-2方向盤-3除了m50d採用m款多功能真皮方向盤外 x5其餘車型皆搭載同樣的跑車多功能真皮方向盤，兩側按鍵組分別可操作音響、儀錶、電話，以及主動車距定速控制系統，後方也沒漏掉實用又可增添駕馭樂趣的換檔撥片。沒錯，即使是入門車型 25d的兩等級也都��齊全的智慧駕駛套件列為標配，除了實用的主動車距定速控制系統外，還包括盲點偵測、車道偏離警示，車側防撞輔助、路口車流防撞輔助、前後方車流警示、後方追撞警示，以及停止、起步時間延長至30秒的壅塞交通輔助功能，至於含駕駛注意力輔助、前座自動束緊式安全帶機能的activeprotection主動安全防護系統也一應俱全。儀錶-1儀錶-2儀錶-3儀錶-4儀錶-5儀錶-612、3吋全數位虛擬座艙不僅讓車室更具科技感，駕駛可依照需求自由切換包括里程、油耗相關數據、檔位、行車模式、g值、引擎即時輸出功率等資訊，中央區域則可顯示先進駕駛輔助系統作動狀況及已列為全車系標配的智能衛星導航。上個月發表的x6當中首度新增的「道路虛擬實境顯示」現在也出現於x5 儀錶可顯示車身四周車道與車輛的即時狀況，車輛圖示則包括轎車、貨車、摩托車，讓駕駛者能更充分地掌握周�������狀況。中控台上的12、3吋多功能觸控螢幕也是全車系標配，下方則為六角型空調介面與出風口、idrive功能快捷鍵等新世代家族設計。螢幕-1螢幕-2螢幕-3螢幕-4螢幕-5中央大螢幕當然也是最新的idrive7、0介面，以精簡的分層式選單、分割畫面等設計讓操作更為直覺，不過25d與其他高階車型的差異則在於少了手勢控制功能。而25d旗艦版包括倒車輔助攝影、360度環景系統、自動停車輔助系統，以及可幫助駕駛在在狹窄巷弄記憶最後50公尺路徑並協助倒車的「自動倒車輔助系統」皆未缺席。座艙關懷程式-1座艙��懷程式-2選單中有一個相當特別的功能稱為「座艙關懷程式」，並有「活力」與「放鬆」兩種選擇，選定後系統將在三分鐘內透過空調、燈光與音樂等系統的變化幫助駕駛者振奮或放鬆心情。新世代車型皆將排檔桿、行車相關按鍵、動態行車模式切換以及idrive手寫觸控板予以整合，讓整個鞍座看來更為簡潔。掀起前方的上蓋，裡頭配置了雙置杯架、12v電源、usb插孔，不過便利的手機無線充電裝置只有xdrive40i旗艦版以上車型才有。試駕車選配了「頂級水晶中控套件」（6萬元），包括排檔頭、idrive控制旋鈕、引擎啟閉按鍵等皆換成了晶瑩剔透的水晶製品，而排檔頭內部還加入了「x」字樣彰顯獨特，為車室增添不少奢華感受。對開式設計的中央扶手內部置物空間還算方正寬敞，並配置一個type-cusb插孔。方向機柱左下方的照明相關控制開關由以往的旋鈕改為按鍵，下方則設計了一個小型置物抽屜。前座-1前座-2前座-3x5xdrive25d於內裝採用了與40i車型相同的vernasca真皮跑車座椅，視覺效果、觸感與乘坐舒適度皆屬出色，而雙前座也皆具備電調、電動腰靠、手動延伸腿靠等機能。後座-1後座-2後座-3後座依舊有寬大的扶手提升乘坐舒適度，內部則附有兩個置杯架以及收納空間。與其他車型相較 25d僅有雙區而非四區恆溫空調系統，因此後座出風口下方以兩個置物凹槽取代液晶空調控制面板，最下則同樣配備一個12v電源插孔。前座空間後座空間上圖是身高177公分乘客入分別置身前後座的空間示意圖，除了膝部、頭部空間充裕，在座椅柔軟度適中、椅背也不會過於直立的狀況下也帶來相當出色的乘坐舒適度。上下兩片對開且皆能電動啟閉的尾廂門對於suv來說是個相當便利的設計，只需開啟上層便能拿取後廂中的小型物品，下層開啟後則可作為延伸平台便於大型物品的進出。後廂-1後廂-2後廂-3x5的後廂具備650公升容積 4、2、4分離後座椅背傾倒後則可擴充至1870公升，掀起底板後下方另有個放置隨車工具的頗大空間。後廂-4後廂-5後廂兩側另配置了實用的後座椅背傾倒拉柄、掛勾、12v電源插孔等配備。正如前文所述 25d動力的出現取代了30d成為目前x5車系唯一之柴油選項，不過縱使由3、0升直列六缸縮減為2、0升四缸配置 25d依舊具備231hp、45、9kgm的充沛出力（之前30d為265hp、63、2kgm），原廠公佈x5xdrive25d的0100km、h加速成績則為7、5秒。搭載了擁有45、9kgm最大扭力的柴油引擎 x5xdrive25d絲毫不將超過兩噸的車重放在眼裡，面對陡坡也能以極低轉速臉不紅氣不喘地一路攀爬直上，較令人驚奇的一點，則是這具四缸柴油引擎的動力湧現方式出奇地順暢，搭配能文能武、表現依舊出色steptronic運動化八速手自排變速箱後，為它帶來不輸高階車型的優異行車質感；若要挑剔的話，則是不若30d一樣踩下油門即有隨傳隨到的扭力，全油門之下也少了些衝勁，另外就是引擎雖已有不錯的運轉精緻度，但與直六動力相較依舊有些微差異。除了外觀、內裝、配備外，操控特質也是newx5另一項令人歡喜的進化，這是我們之前試駕40i車型時得到的結論，這次試駕xdrive25d我們則再一次確認了這件事。雖為入門車型，但原廠還是很有誠意地附上了電子懸吊系統，讓其在日常行駛時可維持舒適乘坐感，面對蜿蜒山也能稱職地支撐車體維持穩定姿態劃過每個彎道，而迅速的轉向反應與精準的車頭指向性更是令人激賞，這些特質對於車身近五米、車重突破兩噸的suv而言可不簡單，也不禁令人想到bmw將x5定義為「sav」（sportactivityvehicle）其實頗為貼切。x5xdrive25d依舊是我們熟悉的x5 也是款表現依舊稱職的豪華suv 因為舒適又寬敞的空間、豐富的豪華與科技配備、出色的運動性等新一代x5的魅力都在xdrive25d身上保留了下來。雖然四缸動力在各方面表現還是不若直列六缸柴油那般出色，但其充沛的出力對於日常使用其實也算遊刃有餘，而且一想到車價由以往30d的343、373萬元一下子降到289、305萬元，bmwx5xdrive25d旗艦版規格諸元表引擎型式，直列四缸dohc16v柴油渦輪排氣量，1995c、c、最大馬力，231hp、4400rpm最大扭力，45、9kgm、1500rpm驅動方式，八速手自排煞車結構，雙a臂後懸吊結構，265、50r19車身尺寸，4922mmx2004mmx1745mm軸距，2070公斤國內售價，305萬元'
    
    # docu = '金色瓶是doretta黃金微導保濕精華，質地像是DHC純橄情煥彩晶華，屬於疣狀的精華，擦在臉上按摩數分鐘立即吸收到肌膚內，今年冬天只靠它完-1全脫離脫皮蕭蕭人生。'
    # docu = '我覺得apple用起跟samsung相比真的差很多，使用起來體驗完全不再同一個檔次。'
    # docu = '最近讚用1028傳酸輛別粉餅 用第一塊 真的不浮粉 臉也打亮了 個人覺得不輸露華濃 可是'
    # docu = 'altima推出一段時間，我發現網路上討論，加上車主社團們車主分享的心得，這台車表現真的算是還不錯，評價也相當好果不其然只要價格開的對了，配備給的夠齊全，對於網友或是買車的人而言就先對了一半不過我也發現說，其實網路上有不少人在敲碗maxima，說是比起altima更希望引進maxima，所以我也好奇研究了一下今天姑且當個鍵盤車評，想跟各位討論一下這兩台車，以及maxima若真的引進，消費者就一定會買單嗎？拉了一下國外maxima的內裝照片看得出來maxima的內裝質感以及整體營造的氛圍走的是比較高端的路線就整體質感來說，以我看過的altima的實車來做對照雖說altima整體的內裝表現我覺得還不錯（尤其我個人又私心偏愛altima的懸浮式螢幕）但我認為，就maxima內裝的感覺整體看下來，確實是有比altima高出一個等級外觀的部分altima在外型上對比過往nissan給消費者比較溫吞的印象，這次的altima確實走的是比較年輕、運動化的外觀不過看到國外maxima的外型以後我才知道，原來還可以更上一層樓搭配車側勾勒出來的線條，也讓maxima就外觀來說成了一台名符其實的運動型房車不過真正吸引大家的應該是這台車核心引擎的魅力吧maxima用的是3.5升v6引擎，這顆引擎在nissan過去市場上相當受到歡迎且最大可以榨出300p的馬力，就馬力數據來說相當出色雖然說altima這次用的vc-turbo引擎同樣也是相當吸引人馬力部分最大能到248p，0-100加速能夠在6.4秒完成油耗表現13.7km/l，以一台近250p的大馬力的車款來說算是相當優異不過我在猜，喜歡這種大馬力車款的人，可能不會太在意油耗表現所以才會不斷敲碗馬力更大的maxima其他，其實兩台車的配備基本上差不多包含說自動煞車（含行人偵測）、隔車追撞警示系統、自動跟車、盲點偵測、rcta、環景系統、車道偏移預防、十氣囊…這些消費者在意的安全與科技配備，兩台車都給的相當完善最後我覺得最大的差異，應該是空間吧，畢竟maxima設定上是真正的大型房車而altima與camry…等等在台灣被很多人稱作大型房車的車款，在國外嚴格定義上應該是中大型房車因此在乘坐空間的表現上altima雖然已經有一定的優勢，但跟maxima對照起來空間就是比較小整體看下來，大概能夠理解為何大家會一直敲碗希望nissan可以引進maxima雖說maxima在設定上好像是比altima高一個層級但有個關鍵點可能會是maxima的硬傷，就是他的排氣量是3.5l過去看版上有些人講到排氣量，別說是3.5l，可能2.5l就已經哀哀叫說稅金很貴而3.5l的稅金，一年可是要比2.0l的稅金多出快2萬元，開十年下來就多了20萬了，的確不是個小數目我在猜原廠可能也是考慮這一點，怕說就算引進，也只是萬人響應一人到場的情形所以最後選擇引進，引擎科技相對比較新、動力油耗表現不錯，科技配備以及內裝質感也不錯的altima畢竟就網友以及消費者過去討論的狀況，我也認為altima的條件會是比較符合台灣市場的不知道大家怎麼看這兩台車？還是說版上還是有人希望nissan引進maxima的以下開放留言「出maxima我就買」'
    # docu = "時代不一樣了 以前要蕭呸買golf r 比買audi還是benz划算現在這年代要蕭呸買m3p 潮又愛地球還不污染空氣 應該說不在當下污染空氣以火電為主的今天還是污染空氣的只是在不同的地點污染地球最大優點是不會吵到鄰居跟引起路人側目喜歡買vag蕭呸又追求cp值的可以等等id.3 r就是了 "
    # docu = "我也是覺得納悶，日規版馬3我有看到也有標配單片cd音響，bose旗艦車型應該要標配單片cd播放器，不然空有那麼好的12支喇叭加重低音，結果用mp3youtube那種數位破壞格式的音源來播放，不是本末倒置嗎?我是覺得有買bose旗艦的車主要跟原廠反映一下，把這個東西列入可事後選配或加購，讓有聽cd需求的人去加購，例如古典樂或是高音質的音樂真的要用cd才能聽出音質的優點，不要白白浪費一套bose音響系統。不然就要車主自己去日本玩的時候順便去日本買帶一台回來裝而已比較麻煩(lexus像ct200基本上還是有給單片cd)台灣版給了一個usb插槽日規版本因為最近在研究，假如七月新年式bose旗艦版看有沒有加回去，如果有cd音響跟電動倚補回去就會加深直上bose版，假如沒有就應該會直接選頂級型就夠了!"
    # docu = "從車廠的角度維修保養是重要收入，用咖啡甜點吸引車主回廠無可厚非但老實講，我覺得lexus矯枉過正多此一舉1.不像雙b，lexus因為零件取得不易，所以外廠很少2.lexus的車主多是不想特別了解車的族群，不太會上網找資料，通常乖乖回原廠3.對車廠而言，一個坐在現場，看著維修單細細檢視的客人容易刪單，還是只能透過電話了解保養內容的客人?我想一定是前者，我如果是車廠，我寧願付200塊計程車抵用卷把客人送走，省下來的冰淇淋甜點咖啡和多出來的保養項目絕對划算4.排擠效應，對於沒有想利用保養維修做lexus下午茶一日遊的車主，看著旁邊一桌桌歡樂無限暢飲的車主，心理作何感想?是，x，林北虧大了，下次我也要呼朋引伴全家出動，還是，我不想跟這些人擁有相同的品牌?不管是哪一種，原廠恐怕都不樂見基於以上四點，我猜想，除了限制點餐，接下來lexus應該也會檢討休息區配置吧?別說我是車主，光是我作為潛在買家，看到那歡樂的景象都默默的走開了。"
    # docu = '看起來市場前十名車款依舊是被toyota包辦多數，不過今年幾台一軍突起的車像是nissan以及ford表現也是非常搶眼，尤其nissan把能把honda擠到第四位，除了獨佔小休旅車市場龍頭外，同時也是整體銷售排行榜前三名的車款，儼然成為了現在市場小休旅車這個級距中的神車。'
    
    
    ### 譬喻與比較語句與連接語句
    # docu = 'sony且厚度也勝過競爭對手samsung S6及蘋果iPhone 6(S6、iPhone6厚度皆為6.9mm。' 
    # docu = '有些人應該沒看過王自如跟羅永浩的視頻對峙大戰吧，蘋果比三星還要好用，在對岸可是針對手機這塊犀利的很少見多怪 三星的底部 指紋 實在太像蘋果了 覺得罵s6+1。'
    # docu = '蘋果比三星還要好用，有些人它，honda比蘋果跟三星還早，'
    # docu = 'honda好像蘋果的性能，'
    # docu = 'wagon系列我覺得skoda比vw好看很多，'
    # docu = '只有我覺得這串是反串嗎ford和vw兩者比妥善率(黑人問號)?然後又是各種萬年dsg老調。是在哈囉。'
    # docu = '目前休旅車，小弟會選擇中華汽車尤其你載老婆載小孩，這台安全性很足夠2.4的排氣量跟toyota 2.0，稅金一樣再來價格也是這台cp值最高省點錢，以後小孩還有很多花費要花我覺得中華汽車的椅子，坐起來最舒服帶老婆去中華汽車的展示間逛逛看，試坐看看吧'
    # docu = '車是負資產，跟房子不同，雖說每個人的現金運用方式不同，但還是建議至少當下要有足夠的現金存款能一次買下整台，再來考慮貸款購車，否則要是後續有突發裝況，那貸款會很有壓力您現在手頭現金不夠覺得有負擔，那就該考慮便宜一點的車子。喜歡進車的話，不妨考慮rav4、tiguan、kodiaq都是空間不錯又便宜一點的選擇。'
    # docu = 'apple不像samsung 那麼好用。'
    # docu = 'apple五我我，跟samsung很像，'
    # docu = '320i touring，這個級距benz，沒有c200t對打能與之對抗的只有audi a4 avant volvo t5 rdesign但這兩個品牌在台灣似乎又比不上bmw看來bmw打了一手好牌，應該可以賣得不錯。'  
    # docu = 'tesla有toyota的性能但火力發電排碳只有油電a等級。'
    # docu = '應該會和 tesla lr，差不多的售價，以 volvo，的品牌力以及看起來不輸 tesla，的性能，空間雖然略小一點，但我想應該還是很有競爭力，至於充電問題嘛。每個人用車需求不一樣，沒有絕對的劣勢啦。'
    # docu = '中華汽車和cross或是mazda等休旅車，只敢抓成績很差的toyota比較，被講honda麋鹿測試成績只比後段班的toyota好，'
    # docu = '亮哥+，wrote:，+1看怡麈的st評測，拉到最後st高速公路的輪拱胎噪不小。價格差不多情況下nissan隔音及空間是大勝ford st，買nissan為的就是空間，像ford bi、golf variant、toyota wagon。等小一號碗公車格，意義不大!+1。同廠牌小一號的wagon，在這棟樓裡面居然大家還認為ford wagon可以賣的比nissan還貴。'    
    # docu = '以 150 萬的預算來說，mazda 是最佳選擇~車內質感和駕駛感覺都屬上等，尤其還配有全套的 adas 系統，而且保養和維修費用也沒有 lexus 來的高~但是，如果要 cp 值的觀點來看，其實 toyota 油電版的 cp 值真的很高，光未來的燃油使用費用，就比 mazda 少很多~理性分析下，我個人推薦你拿這兩個選項給你長輩來選~'
    # docu = '認真跟你講，經過這近二十年，各車廠間在品質、可靠度等，是有消長的。不容易改變的，其實是消費者對其印象。我爸以前計程車開ford20年都沒進步，現在的20年就會進步，三不五時就有人想挑戰神a 還不是都被打趴，唯一贏神a的就只有自已的toyota而已，我是真心希望出一個跟神a競爭的車子，讓消費者有選擇，不要嘴巴講講而已。'
    # docu = '目前休旅車，小弟會選擇中華汽車尤其你載老婆載小孩，這台安全性很足夠2.4的排氣量跟toyota 2.0，稅金一樣再來價格也是這台cp值最高省點錢，以後小孩還有很多花費要花我覺得中華汽車的椅子，坐起來最舒服帶老婆去中華汽車的展示間逛逛看，試坐看看吧。'
    # docu = '現行款ford 245旗艦型2.0t 應該比較適合樓主，動力也比得上saab93.，'
    # docu = '其實這車一直有吃機油的問題，從開始的不吃機油，再來七八千吃一瓶，四五千吃一瓶，一千吃一瓶(9萬)，最後「進步」到500吃一瓶(12萬公里)，達到1l2000 km的臨界值，發動機就該整理了!正常使用下，每15000 km 消耗0.5l以內的機油是正常的!全程d檔?，不但dsg變速箱的離合器及tcm容易提早畢業!發動機缸溫不足，+積碳，發動機也需提早整理(正常使用下可到30萬公里).，全程d檔真的問題多多!這樣的例子，網上不少見!某b牌性能化品牌，搭at自排變速箱，正常掛檔模式全程d檔好棒棒，但發動機積碳，吃機油不可少 at自排變速箱又不耐用.，這也是vag堅持dsg的初衷，渦輪發動機+dsg的組合，正常使用下，發動機及dsg都可以很耐用!vag，bmw，subaru等三家以性能化為主的車企，發動機缸溫不足，都容易過度消耗機油.，樓主駕車掛檔模式。大部分也只是上下班開，從不大腳油門。這句話我看了，就知道數年後的結果了!性能化的設計，轉速最好不要長時間連續處於2000 rpm以下.。'
    # docu = 'apple、samsung 用起來的感覺跟比sony 好很多。'
    
    
    '''單主題或許也要進行第一次切句
    '''
    # docu ='其實wagon的實用性真的比四門車好，我也喜歡因為低底盤可以擁有比休旅車更好的操控，但是我一路觀察到現在，同樣有四門或是wagon的車款，都是四門比較好看?僅有少數車款是我覺得wagon造型不輸給四門的，像audi就是'
    # docu = 'infiniti輛廠牌年份型號:toyota2020年式2020出廠汽油尊爵是否含保險:否是否含領牌稅金:含成交價格(空infiniti價格:87.89.3下訂日期範圍:229尋infiniti地點:嘉義保險類別金額:乙式男or女自費配備配件:無贈送配備配件:全infiniti隔熱紙、避光墊、防水腳墊、後箱防水墊、廣角鏡、交infiniti美容、鑰匙皮套、etc付款方式(現金or貸款):貸款自費規費總額:動保費4000購infiniti心得:騎著infiniti要去吃晚餐路上路過問了一下，看板上沒有汽油尊爵版的單，不知道這單可以嗎?原諒蔡逼八，業務把影音特仕寫在購買配件那我就照打'
    # docu = '注意:發文請詳讀板規1問infiniti文規範問infiniti緣由必須超過30字如無照片請詳述infiniti輛外觀超過100字，並以網路圖片或是草圖加以敘述問infiniti緣由:今天在我們daihatsu財市看到的infinitimurano隔壁那輛黑infiniti(其實murano也是第一次看到可能以前都沒注意吧)照片顯示為infiniti輛內裝或是外觀:照片 '
    # docu = '為啥你老是覺得把汽車 因為我覺得機車最近出的除了symfxn外，幾乎都千篇一率.改殼、外觀、和只會了七期的環保更新外，到底有啥特別加速多一點點就爽快了?yamaha.至少有tricity.跟nikengt.跟兩輪油電grandfilanohybridabs但光陽呢。始終都沒進步然後油電原本就貴，電池貴.純油的70w，最便宜的toyotaphev也要140w，所以價錢原本就不是考量想用6萬買油電機車?就跟60萬買油電汽車一樣?拿mach比toyota的概念?三輪汽油都20萬了但電池現在都月租反而是好事，因為買斷車廠要負擔8年15萬公里保固也很困難所以nissanepower的超小電池就是一個折衷方案.無論是價錢+空間.不用大電池改成油箱，就能有很大的空間了'
    # docu = '目休旅車小弟會選擇中華汽車尤其你載老婆載小孩這台安全性很足夠2.4的排氣量跟toyota2.0稅金一樣再來價格也是這台cp值最高省點錢以後小孩還有很多花費要花我覺得中華汽車的椅子坐起來最舒服帶老婆去中華汽車的展示間逛逛看試坐看看吧'
    # docu = '如果honda 866和toyota/nissan)benzc300&honda benz出個國產和旅行車。價錢合理，一定有固定的銷售量 bi當初超想買，最後妥善率讓我不敢下手，'
    # docu = '100萬左右就直上 rav4，爸媽坐後座也較舒適'
    # docu = '他買toyota不如買它它它lamborghini。'
    # docu = 'bmw=benz 大讚 1=2'
    # docu = 'toyota價格有優勢 mazda0內裝有優勢。'
    # docu = 'honda在台灣只有擺爛 honda被toyota屌打也不易外，收回代理也沒改變，只有價格變貴而已!'
    # docu = 'bmw或者benz。，你所說的5及e的大改款全新開發需要花費大量資源情況，應該是從0開始開發。不過toyota/lexus es是在與toyota相同引擎與底盤基礎上，再調校優化，應該不是從0開始。同理，既然新世代的tnga中大型後驅房車crown已經是現成的了，再從相同的基礎上推出gs 理應也不需要從0開始，我是這麼想。'
    # docu = 'kate蹤影立體:kate的淺咖啡納格跟我原本Dior#796的咖啡金很像，所以不考慮。本來想買kate的香檳金，但後來試色跟media#BR-01很像，價格上考量買了media，'
    # docu = '平常就很喜歡看這系列的我決定也來發一下自己平常上學的包包都放什麼?(第一次發文可能有點難看請見諒?)首先，我的包包是很久以前高中的時候outdoor特價買的顏色超級粉嫩(?)而且很耐重!(包包一定要掛上吊飾啊!可愛的史黛拉?(?)?再來是我的化妝包(??ω?)?身為一個比熊愛好者，連化妝包都要用比熊的但有時候要裝的東西比較多我就會換成小美人魚那個 0.jpeg?藍色的the saem 護手霜:身上一定要隨時帶著一條護手霜!手乾乾的時候馬上就要塗，這個是香水護手霜所以味道超級香(???)艾杜紗24小時美顏蜜粉(不知道是不是這個名字抱歉(?身為一個額頭容易出油(然後瀏海就會跟著油膩膩)的女子，蜜粉是隨時隨地都必須帶在身上的!這個蜜粉的味道超級好聞?是一種很香的寶寶味??i’mmeme 我愛雪酪22 concept eyes這個是最近的新歡!(?ω?)?很好用，光澤很美而且包裝也超級美的?其他就是我一定不能不帶的鏡子，蜂凡士林紅(題外話 romand的唇釉很好用(???)還有無印睫毛夾!接下來是一些跟手機有關的小東西們?(?ω?)之前在日本plaza買的泡泡先生的藍芽耳機，還有蝦皮買的很方便又很可愛的充電線跟我的行充(兔子跟刺蝟是我自己貼上去的?)，b2ab81479add48ea1c2/640.jpeg再來!是上學必備倩碧?大家應該很常看到的超級可愛夾心餅乾哈哈哈哈?b2b2d74af2ad7ec5/640.jpeg我覺得很重要的是可以折疊的剪刀跟snoopy釘書機這真的超級方便(?ω?)而且完全不佔位子?還有大創的彩色標籤，我也會隨身帶?然後通勤族最重要的票卡夾，也是狗狗的?!?最後是我的小小保溫瓶跟比熊的杯套?我習慣喝熱的所以都帶保溫瓶?雖然這容量很小要一直裝水但是snoopy真的很可愛!因為太怕刮到所以一定要裝杯套(???)，以上就是我的what’s in my bag 分享啦?(?)(好像漏掉很多小東西到時候再補?感謝大家觀看我極為不專業的分享。'
    # docu = 'honda的性能、操控、再加速反應都贏toyota，入手價也比toyota便宜二十幾萬了，不知道這樣算不算霸主?喔對了，上述是指toyota 2.5，不是台灣規的2.0喔。'
    # docu = 'la prairie La Prairie可以試看看，建議要買一整套，這樣效果會比較好另外也可以參考chanel Chanel，最近還出了新的同系列化妝水和La Mer，趁母親節趕快買兩套，等週年慶又可以補貨了。'
    # keywords = findall(r'(la prairie|la prairie|la mer|avene|curel|lenux|mac|neutrogena|多容安|chanel|benefique|anessa|bifesta|origins|la roche─posa|revlon|m.a.c|dior|innisfree|avene|aness|darphin|lancome|ahc|rmk|touch|王子|妙兒褲|麗貝樂|妙兒舒|幫寶適|大王|滿意白金|妙而舒|一級幫|好奇|舒潔|滿意寶寶|za|darphin|labo|勞斯萊斯|kamiq|citroen|愛快|media|kate|dior|kamiq|jaguar|opel|peugeot|subaru|雙田|saab|資生堂|佳麗寶|專科|露得清|巴黎萊雅|cross|luxgen|裕隆|kia|carens|hyundai|vag|amarok|法拉利|ferrari|三菱|福特|urus|model y|suzuki|porsche|xc60|bentley|lamborghini|tiguan|volvo|mitsubishi|honda|ford|mazda|雙b|skoda|vw|ranger|hilux|rav4|kodiaq|中華汽車|daihatsu|infiniti|toyota|1028|倩碧|三星|蘋果|lexus|audi|nissan|汎德|bmw|鈴木|台本|honda|百優|蘭寇|samsung|apple|sony|htc|naras|雅詩蘭黛|bmw|benz|ipad|doretta|dhc|tesla)', docu.lower())
    

    sg = Segment('benz比bmw 好很多。', {'benz', 'bmw'})
    sg.second_step_cut()



