import pandas as pd
import Variables
import requests
from DataExtract import *
import concurrent.futures
from multiprocessing import Process, Pool, cpu_count
import logging 
from Cleaning import ClearContent
from flashtext import KeywordProcessor



logger = logging.getLogger("情緒分析")
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s : %(message)s')

class Preprocessor:
    def __init__(self, industry):
        self.industry = industry
        self.ids = self.get_channl_ids()
        self.pro_keys = self.get_pro()
        self.keywords = self.get_keyword()


    def get_channl_ids(self):
        with MongoConnection() as conn: 
            conn.change_db('Indexasia')
            chann = list(conn.get_all(table_name='industries', conditions={'industry_name':  self.industry}, showfield={'_id': 0, 'channel_id':1}))
            cha_id = [v for i in chann for k, v in i.items()]

            chann = list(conn.get_all(table_name='channel', conditions={'_id': {'$in': cha_id}, 'tw_type':'forum'}, showfield={'_id':1})) # targeting on forum type for now
            ids = [str(value) for j in chann for key, value in j.items() if key == '_id']

        return ids

    def get_pro(self):
        path_pro = Variables.get_path_pro( self.industry)
        df = pd.read_excel(path_pro)
        pro_list = df[['品牌關鍵字', '產品關鍵字']].values
        final_list = list(set([j for i in pro_list for j in i]))

        return final_list

    def get_keyword(self):
        path_keyword = Variables.get_path_keywords(self.industry)
        subject_df = pd.read_excel(path_keyword, usecols= ['品牌名稱', '產品名稱'])
        subject_df = subject_df.astype(str)
        keyword_list = subject_df.values

        return keyword_list


    def get_and_clean_data(self, startTime, endTime):
        ''' get data from johnny api, done data cleaning and unifying the keys benz and eventually list  out the unique subjects
        '''
        with concurrent.futures.ProcessPoolExecutor(max_workers = cpu_count()) as executor:
            init_df = pd.DataFrame()
            logger.info('開始用多進程拉資料!')
            futures = [executor.submit(call_api, self.ids, self.pro_keys[i], startTime, endTime) for i in range(len(self.pro_keys))]
            final = []
            for future in concurrent.futures.as_completed(futures):
                final.append(future.result())
                logger.info('完成!')
            # print(final)
            for i in range(len(final)):
                try:
                    df = pd.DataFrame(final[i])
                    init_df = pd.concat([init_df, df], axis = 0, ignore_index= True )
                    init_df.reset_index(drop= True, inplace = True)

                except (KeyError,TypeError,ValueError):
                    logger.info(f'fail ! {i}')

        init_df.drop_duplicates(subset = 'content', inplace= True)
        cn = ClearContent()
        logging.disable()
        result = cn.df_input(init_df) 

        return result

    def extract_subjects(self, content_df, tolerance_switch = True):
        pro_list = self.keywords
        # print(pro_list)
        pre_list = [j.split('+') for i in self.keywords for j in i]
        post_list = [j.lower() for i in pre_list for j in i]
        keyword_processor = KeywordProcessor()
        keyword_processor.add_keywords_from_list(list(set(post_list)))

        # # deal with za vs plaze
        for j in range(len(pro_list)):
            new_string = '' 
            inputt1 = self.keywords[:,0][j].split('+')
            new_list = []
            for k in inputt1:
                if k.encode('utf-8').isalpha(): # 判斷都為英文
                    new_string = f'[^a-z]{k}[^a-z]|^{k}|{k}$'
                    new_list.append(new_string)
                else:
                    new_list.append(k)

            pro_list[:,0][j] = '+'.join(new_list)

        # 根據不同產業，有些產頻名稱需要字元跟字元間可以容忍其他字元像是美妝跟母嬰 e.g. Click翻糖氣墊唇頰筆 -> C l i c k 翻 糖 氣 墊 唇 頰 筆
        for j in range(len(pro_list)): 
            # deal with za vs plaze
            if tolerance_switch:
                inputt = re.sub('[（）（\(\)]', '',  pro_list[:,1][j].strip())
                new_string2 = ''
                # print(inputt)
                for i in range(len(inputt)):
                    if i == 0 :
                        new_string2 += inputt[i]

                    # 英 英
                    elif re.search('[a-zA-Z]', inputt[i-1]) and re.search('[a-zA-Z]', inputt[i]) :
                        new_string2 += '.{0,1}' +inputt[i] 
                        
                    # 英 數
                    elif re.search('[a-zA-Z]', inputt[i-1]) and re.search('[0-9]', inputt[i]) :
                        new_string2 += '.{0,1}' + inputt[i] 

                    # 數 數
                    elif re.search('[0-9]', inputt[i-1]) and re.search('[0-9]', inputt[i]) :
                        # new_string2 += '.{0,1}' + inputt[i]
                        new_string2 +=  inputt[i]

                    # 數 英
                    elif re.search('[0-9]', inputt[i-1]) and re.search('[a-zA-Z]]', inputt[i]) :
                        new_string2 += '.{0,1}' + inputt[i] 
    
                    # 數 符號
                    elif re.search('[\u4e00-\u9fa50-9]', inputt[i-1]) and re.search('[\.]', inputt[i]) :
                        new_string2 += '.{0,1}' +  '\\' + inputt[i]


                    # 英 符號
                    elif re.search('[a-zA-Z]', inputt[i-1]) and re.search('[\.]', inputt[i]) :
                        new_string2 += '.{0,1}' +  '\\' + inputt[i] 

                    else:
                        new_string2 += '.{0,2}' + inputt[i] 
                    pro_list[:,1][j] = new_string2 
            else:
                pass
        # 從後到前
        final_list = []
        string = ''
        for i in range(len(pro_list)):
            # print(self.keywords[i])
            if i == 0 :
                string += pro_list[i][1] + '|'  + pro_list[i][0] 
                if i == len(pro_list) -1: # 處粒最後個element 時
                    final_list.append(string)          

            elif pro_list[i][0] in string :
                string =  pro_list[i][1] + '|' + string
                if i == len(pro_list) -1: # 處粒最後個element 時
                    final_list.append(string)

            elif pro_list[i][0] not in string:
                final_list.append(string)
                string = ''
                string += pro_list[i][1] + '|'+ pro_list[i][0]
                if i == len(pro_list) -1: # 處裡最後個element 時
                    final_list.append(string)

        final_list = list(map(lambda x: x.replace('+', '|'), final_list))
        final_list = list(map(lambda x: x.replace('||', '|'), final_list))
        final_list = list(map(lambda x: x.lower(), final_list))
        def customize(data):
            ''' 把主題統一'''
            data = data.lower()
            
            for i in final_list:
                each_re = re.sub('\[\^.+z\]\|', '', i) # 把[^a_z] …[^a_z]  拿掉
                if re.search(each_re, data):
                    for j in each_re.split('|'):
                        if '.{0,' not in j  and '[^a-z' not in j and not re.search('[\^\$’\. 0-9]', j): # e.g. space between 'estee lauder'之後斷詞關西                      
                            j = re.sub('[\^\$]', '', j)

                            data = re.sub(each_re, ' ' + j, data) # 前面加空格因為pass到cut function 會找不到subject  e.g 再搭neutrogenaneutrogena洗
                            break
            return data

        content_df['clean_data'] = content_df['clean_data'].astype(str)
        content_df['clean_data'] = content_df['clean_data'].apply(customize) # unifying the keys benz, 賓士 --> benz
        
        key2reg = '|'.join(final_list)
        def combine_set(text):
            text = text.lower()
            re_set = set(re.findall(key2reg.lower(), text))
            re_set = set(filter(lambda x: x != '', re_set))
            re_set = set(map(lambda b: ''.join(re.findall('[a-z\-]+', b)) if re.search('[\u4e00-\u9fa5]*[a-z]+[a-z][\u4e00-\u9fa5]*', b) and not re.search('[a-z\u4e00-\u9fa5]+[ ─-][a-z\u4e00-\u9fa5]+', b) else b, re_set))
            keyword_set = set(keyword_processor.extract_keywords(text)) # 處理黏在一起 avene跟anessa 與 plaza vs za 與  macm.a.c(這情況不會抓到)
            final_set = re_set | keyword_set
            # print(keyword_set)

            return set(final_set)

        content_df['subjects'] = content_df['clean_data'].apply(combine_set)

        return content_df


    def remove_empty_subjects(self, df):
        ''' dealing with no empty subject keys
        '''
        information = df.to_dict('records')
        information_copy = information.copy()
        for i in range(len(information)):
            for j in range(len(information_copy)):
                if (information[i]['main_id'] == information_copy[j]['main_id']) and (information_copy[j]['reply_id'] == '0'):
                    information[i]['post_subjects'] = information_copy[j]['subjects']

        dff = pd.DataFrame(information)
        dff = dff.fillna('nan')
        without_any_keys = []
        info_list = dff[['subjects', 'post_subjects']].values
        for i in range(len(info_list)):
            if len(info_list[i][0]) == 0 and info_list[i][1] == 'nan':
                without_any_keys.append(i) 
            elif len(info_list[i][0]) == 0 and len(info_list[i][1]) == 0:
                without_any_keys.append(i)       

        dff.drop(without_any_keys, inplace= True)
        dff.reset_index(drop=True, inplace=True) 

        return dff

def call_api(ids, pro_key, startTime, endTime):
    data = {"start_time": startTime, "end_time": endTime, 'channels': ids, 'channel_is_id': True, 'keywords': pro_key ,'search_place': 'comment', 'search_field': ['title', 'content', 'comment'], 'just_find': True}

    raw_data = requests.post('http://api.i-buzz-system.com/load-data/api/v2/search_topic', json = data)
    data = raw_data.json()

    return data

if __name__ == '__main__':
    pr = Preprocessor('汽車') # 汽車 美妝 母嬰
    # pr.ids
    # pr.pro_keys

    # clean_df = pr.get_and_clean_data('2020-05-08', "2020-05-09")
    
    # 這邊可以用自己的資料, 並不一定要從資料庫抓
    clean_df = pd.DataFrame([{'clean_data': 'benz 比bmw 好很多。'}])
    new_df = pr.extract_subjects(clean_df, True)
    # df = pr.remove_empty_subjects(new_df)
    # print(new_df.values)

    # k = pr.remove_empty_subjects(new_df)
    # pr.pro_keys

