import Variables
from Preprocessing import Preprocessor
from Segmentation import Segment
import pandas as pd



class ContentDirectivity:
    def __init__(self, industry):
        self.preprocessor = Preprocessor(industry)
        # self.input_df = self.get_input_df()
        # self.output_df = self.get_output_df()

    def get_input_df(self, startTime, endTime):
        clean_df = self.preprocessor.get_and_clean_data(startTime, endTime)
        new_df = self.preprocessor.extract_subjects(clean_df, True) 
        df = self.preprocessor.remove_empty_subjects(new_df)

        return df


    def get_output_df(self, input_df):
        df = input_df.rename(columns = {'content': 'original'})
        new_df = df[['reply_id', 'original', 'clean_data', 'subjects', 'post_subjects']]
        trial = new_df.to_dict('records')

        each_group = []
        for i in range(len(trial)):
            try:
                sg = Segment(trial[i]['clean_data'].lower(), list(trial[i]['subjects']))
                product = sg.second_step_cut()
                for k in product:
                    k.update(trial[i])
                    each_group.append(k)
            except:
                print('error: ' ,trial[i])

        ddf = pd.DataFrame(each_group)
        ddf = ddf.drop_duplicates(subset = 'content')

        return ddf

    def pipeline(self, content):
        clean_df = pd.DataFrame([{'clean_data': content}])
        new_df = self.preprocessor.extract_subjects(clean_df, True) 

        trial = new_df.to_dict('records')
        each_group = []
        for i in range(len(trial)):
            try:
                sg = Segment(trial[i]['clean_data'].lower(), list(trial[i]['subjects']))
                product = sg.second_step_cut()
                each_group.append(product)

            except:
                print('error: ' ,trial[i])

        return each_group



if __name__ == '__main__':
    cd = ContentDirectivity('汽車')
    # print(cd.pipeline('Mitsubishi Grand Lancer 1.8 魅力型 車輛狀況：2020年式新車（不要試乘車、展示車） 交易地區：北、中部 聯絡方式：站內私訊 1. 空車最大折價 2. 可贈送配件項目 3. Grand Lancer 若加多功能駕駛環景輔助系統 4. 加尾翼 5. 請私訊報價+名片\n'))
    print(cd.pipeline('benz比bmw 好很多。'))
    # input_df = cd.get_input_df('2020-05-08', '2020-05-09')
    # output_df = cd.get_output_df(input_df)

    # output_df.to_excel('check.xlsx')
